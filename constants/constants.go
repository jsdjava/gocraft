package constants

const (
	WATER_CUBE_INDEX   byte = 0
	GRASS_CUBE_INDEX   byte = 1
	SAND_CUBE_INDEX    byte = 2
	WOOD_CUBE_INDEX    byte = 3
	STONE_CUBE_INDEX   byte = 4
	DIRT_CUBE_INDEX    byte = 5
	BEDROCK_CUBE_INDEX byte = 6

	GOLD_CUBE_INDEX     byte = 7
	IRON_CUBE_INDEX     byte = 8
	DIAMOND_CUBE_INDEX  byte = 9
	REDSTONE_CUBE_INDEX byte = 10
	LAPIS_CUBE_INDEX    byte = 11
	COAL_CUBE_INDEX     byte = 12
)
