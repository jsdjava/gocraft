package main

import (
	"fmt"
	"math"
	"time"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

// TODO Will probably eventually move logic into player class and repurpose it to include
// this

var crossHairsVertexShaderStr = `
#version 410 core

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 inTexCoord;

out vec2 texCoord;

void main()
{
  gl_Position = vec4(pos,-1,1.0);
  texCoord = inTexCoord;
}
` + "\x00"

var crossHairsFragmentShaderStr = `
#version 410 core

uniform sampler2D ourTexture;

in vec2 texCoord;
out vec4 FragColor;

void main()
{
  
  vec4 texColor = texture(ourTexture,texCoord);
  if (texColor.a < 0.1){
	  discard;
  }
  FragColor = texColor;
}
` + "\x00"

var crossHairsProg uint32

func initCrossHairShaders() {
	vertexShader, err := compileShader(crossHairsVertexShaderStr, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}
	fragShader, err := compileShader(crossHairsFragmentShaderStr, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}
	crossHairsProg = gl.CreateProgram()
	gl.AttachShader(crossHairsProg, vertexShader)
	gl.AttachShader(crossHairsProg, fragShader)
	gl.LinkProgram(crossHairsProg)
	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragShader)
}

// dead simple way to bail myself out of z-fighting, just make the hovered cube a tiny bit bigger
const hcSize float32 = 1.05

func (h *HoveredCube) updateVertices() {
	h.vertices = []float32{
		// close face
		h.pos.X() + hcSize, h.pos.Y() + hcSize, h.pos.Z() + hcSize, h.tex.x2, h.tex.y2, //0
		h.pos.X() + hcSize, h.pos.Y(), h.pos.Z() + hcSize, h.tex.x2, h.tex.y1, //1
		h.pos.X(), h.pos.Y(), h.pos.Z() + hcSize, h.tex.x1, h.tex.y1, //2
		h.pos.X(), h.pos.Y() + hcSize, h.pos.Z() + hcSize, h.tex.x1, h.tex.y2, //3

		// far face
		h.pos.X() + hcSize, h.pos.Y() + hcSize, h.pos.Z(), h.tex.x1, h.tex.y2, //4
		h.pos.X() + hcSize, h.pos.Y(), h.pos.Z(), h.tex.x1, h.tex.y1, //5
		h.pos.X(), h.pos.Y(), h.pos.Z(), h.tex.x2, h.tex.y1, //6
		h.pos.X(), h.pos.Y() + hcSize, h.pos.Z(), h.tex.x2, h.tex.y2, //7

		// eventually, this will probably be a different texture (a lot of the time)
		//top face
		h.pos.X() + hcSize, h.pos.Y() + hcSize, h.pos.Z(), h.tex.x2, h.tex.y2, //8
		h.pos.X(), h.pos.Y() + hcSize, h.pos.Z(), h.tex.x1, h.tex.y2, //9
		h.pos.X(), h.pos.Y() + hcSize, h.pos.Z() + hcSize, h.tex.x1, h.tex.y1, //10
		h.pos.X() + hcSize, h.pos.Y() + hcSize, h.pos.Z() + hcSize, h.tex.x2, h.tex.y1, //11

		// bottom face
		h.pos.X() + hcSize, h.pos.Y(), h.pos.Z(), h.tex.x2, h.tex.y2, //12
		h.pos.X(), h.pos.Y(), h.pos.Z(), h.tex.x1, h.tex.y2, //13
		h.pos.X(), h.pos.Y(), h.pos.Z() + hcSize, h.tex.x1, h.tex.y1, //14
		h.pos.X() + hcSize, h.pos.Y(), h.pos.Z() + hcSize, h.tex.x2, h.tex.y1, //15
	}
}

var crossHairsVao uint32

func makeCrossHairs(tex Texture) {
	// selected cube
	hoveredCube.tex = &tex
	hoveredCube.updateVertices()
	hoveredCube.indices = []uint32{
		// close face
		0, 2, 1,
		0, 3, 2,
		//far face
		4, 5, 6, //7, 6, 5,
		4, 6, 7, //4, 7, 5,
		//top
		8, 9, 10, //4, 3, 0, //0, 3, 4,
		8, 10, 11, //4, 7, 3, //3, 7, 4,
		//bottom
		14, 13, 12, //5, 1, 2, //1, 2, 5,
		12, 15, 14, //5, 2, 6, //5, 6, 2,
		//right
		4, 1, 5, //0, 1, 5,
		4, 0, 1, //0, 4, 5,
		//left
		7, 6, 2, //3, 7, 2,
		7, 2, 3, //7, 6, 2,
	}

	var hcEbo uint32
	gl.GenVertexArrays(1, &hoveredCube.vao)
	gl.BindVertexArray(hoveredCube.vao)
	gl.GenBuffers(1, &hoveredCube.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, hoveredCube.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(hoveredCube.vertices), gl.Ptr(hoveredCube.vertices), gl.DYNAMIC_DRAW)
	gl.GenBuffers(1, &hcEbo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, hcEbo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(hoveredCube.indices)*4, gl.Ptr(hoveredCube.indices), gl.STATIC_DRAW)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 5*4, nil)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 5*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)

	// crosshairs
	var vbo uint32
	var ebo uint32
	chRadius := float32(0.03)
	var vertices = []float32{
		-chRadius, -chRadius, tex.x1, tex.y1,
		chRadius, -chRadius, tex.x2, tex.y1,
		chRadius, chRadius, tex.x2, tex.y2,
		-chRadius, chRadius, tex.x1, tex.y2,
	}
	var indices = []uint32{
		2, 3, 0,
		2, 0, 1,
	}
	gl.GenVertexArrays(1, &crossHairsVao)
	gl.BindVertexArray(crossHairsVao)
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(vertices), gl.Ptr(vertices), gl.STATIC_DRAW)
	gl.GenBuffers(1, &ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indices)*4, gl.Ptr(indices), gl.STATIC_DRAW)
	gl.VertexAttribPointer(0, 2, gl.FLOAT, false, 4*4, nil)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 4*4, gl.PtrOffset(2*4))
	gl.EnableVertexAttribArray(1)
}

const (
	ACTION_IDLE       = 0
	ACTION_BUILDING   = 1
	ACTION_DESTROYING = 2
)

var actionState = ACTION_IDLE

type HoveredCube struct {
	hasPrev  bool
	prevPos  mgl32.Vec3
	pos      mgl32.Vec3
	selected bool
	tex      *Texture
	vertices []float32
	indices  []uint32
	vao      uint32
	vbo      uint32
}

const hoverDepth = 6

var hoveredCube HoveredCube

func floorF32(x float32) float32 {
	return float32(math.Floor(float64(x)))
}

func floorVec3(v mgl32.Vec3) mgl32.Vec3 {
	return mgl32.Vec3{floorF32(v.X()), floorF32(v.Y()), floorF32(v.Z())}
}

var buildCoolTime = int64(0)

func startBuilding(
	world *World,
	playerX float32, playerY float32, playerZ float32,
	playerWidth float32, playerHeight float32, playerDepth float32,
) {
	if actionState != ACTION_DESTROYING {
		actionState = ACTION_BUILDING
		if time.Now().Unix()-buildCoolTime > 1 && hoveredCube.selected && hoveredCube.hasPrev {
			if world.buildCube(hoveredCube.prevPos, playerX, playerY, playerZ, playerWidth, playerHeight, playerDepth) {
				buildCoolTime = time.Now().Unix()
			}
		}
	}
}

func stopBuilding() {
	if actionState == ACTION_BUILDING {
		actionState = ACTION_IDLE
	}
	buildCoolTime = 0
}

var destroyStartTime = int64(0)

func startDestroying(world *World) {
	if actionState != ACTION_BUILDING {
		actionState = ACTION_DESTROYING
		if destroyStartTime == 0 || !hoveredCube.selected {
			destroyStartTime = time.Now().Unix()
		} else if time.Now().Unix()-destroyStartTime >= 1 {
			fmt.Print("Destroying cube \n")
			world.destroyCube(hoveredCube.pos)
			destroyStartTime = 0
		}
	}
}

func stopDestroying() {
	if actionState == ACTION_DESTROYING {
		actionState = IDLE
		destroyStartTime = 0
	}
}

// TODO Really need to wrap this in a struct, this code sucks
// note, if the world hasn't finished loading, then this won't properly track (also until the mouse moves)
func crossHairsOnMove(world *World, projectionMatrix mgl32.Mat4, viewMatrix mgl32.Mat4, cameraPos mgl32.Vec3) {
	// I don't totally understand the math behind this, but the idea is we take our screen position (its the center, so 0,0),
	// and then we multiply that first by p-inv, and then by v-inv to get our world coordinates. looking at the math on paper,
	// i think i could do this a lot more efficiently because <0,0> causes a lot of terms to drop out
	// im not quite sure on why the z/w are picked to be -1 and 1, but the z has no impact on the final result
	// the goal is to get a ray in world space that we can run starting from camera pos to find the nearest block hitting the cursor
	rayEye := projectionMatrix.Inv().Mul4x1(mgl32.Vec4{0, 0, -1, 1})
	rayEye[2] = -1 // suspect it already is this
	rayEye[3] = 0
	rayWorld := viewMatrix.Inv().Mul4x1(rayEye)
	unitRayWorld := mgl32.Vec3{rayWorld.X(), rayWorld.Y(), rayWorld.Z()}.Normalize()
	hoveredCube.hasPrev = false
	for i := 0; i < hoverDepth; i++ {
		curPos := unitRayWorld.Mul(float32(i)).Add(cameraPos)
		//fmt.Print(curPos)
		//fmt.Print("\n")
		if world.filled(curPos.X(), curPos.Y(), curPos.Z()) {
			hoveredCube.selected = true
			hoveredCube.pos = floorVec3(curPos)
			hoveredCube.updateVertices()
			//	fmt.Print(hoveredCube.pos)
			//	fmt.Print("\n")
			//	fmt.Print(hoveredCube.selected)
			//	fmt.Print("\n")
			return
		} else {
			hoveredCube.hasPrev = true
			hoveredCube.prevPos = floorVec3(curPos)
		}
	}
	hoveredCube.selected = false
}

func drawCrossHairs() {
	if hoveredCube.selected {
		// update vertices to match the current player position
		gl.BindBuffer(gl.ARRAY_BUFFER, hoveredCube.vbo)
		gl.BufferData(gl.ARRAY_BUFFER, 4*len(hoveredCube.vertices), gl.Ptr(hoveredCube.vertices), gl.DYNAMIC_DRAW)
		// actually draw everything
		gl.BindVertexArray(hoveredCube.vao)
		gl.DrawElements(gl.TRIANGLES, int32(len(hoveredCube.indices)), gl.UNSIGNED_INT, nil)
	}
	gl.UseProgram(crossHairsProg)
	gl.BindVertexArray(crossHairsVao)
	gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, nil)
}
