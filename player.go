package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

type Player struct {
	x        float32
	y        float32
	z        float32
	width    float32
	height   float32
	depth    float32
	vertices []float32
	indices  []uint32
	vbo      uint32
	vao      uint32
	tex      *Texture
}

func (p *Player) getCameraPos() mgl32.Vec3 {
	// camera needs to be centered
	// TODO instead of 0.5, should probably be scaled by width/2, height/2, depth/2
	return mgl32.Vec3{p.x + p.width/2, p.y + p.height/2, p.z + p.depth/2}
}

func (p *Player) moveX(v float32) {
	p.x += v
	p.updateVertices()
}

func (p *Player) moveZ(v float32) {
	p.z += v
	p.updateVertices()
}

func (p *Player) moveY(v float32) {
	p.y += v
	p.updateVertices()
}

func (p *Player) updateVertices() {
	p.vertices = []float32{
		// close face
		p.x + p.width, p.y + p.height, p.z + p.depth, p.tex.x2, p.tex.y2, //0
		p.x + p.width, p.y, p.z + p.depth, p.tex.x2, p.tex.y1, //1
		p.x, p.y, p.z + p.depth, p.tex.x1, p.tex.y1, //2
		p.x, p.y + p.height, p.z + p.depth, p.tex.x1, p.tex.y2, //3

		// far face
		p.x + p.width, p.y + p.height, p.z, p.tex.x1, p.tex.y2, //4
		p.x + p.width, p.y, p.z, p.tex.x1, p.tex.y1, //5
		p.x, p.y, p.z, p.tex.x2, p.tex.y1, //6
		p.x, p.y + p.height, p.z, p.tex.x2, p.tex.y2, //7

		// eventually, this will probably be a different texture (a lot of the time)
		//top face
		p.x + p.width, p.y + p.height, p.z, p.tex.x2, p.tex.y2, //8
		p.x, p.y + p.height, p.z, p.tex.x1, p.tex.y2, //9
		p.x, p.y + p.height, p.z + p.depth, p.tex.x1, p.tex.y1, //10
		p.x + p.width, p.y + p.height, p.z + p.depth, p.tex.x2, p.tex.y1, //11

		// bottom face
		p.x + p.width, p.y, p.z, p.tex.x2, p.tex.y2, //12
		p.x, p.y, p.z, p.tex.x1, p.tex.y2, //13
		p.x, p.y, p.z + p.depth, p.tex.x1, p.tex.y1, //14
		p.x + p.width, p.y, p.z + p.depth, p.tex.x2, p.tex.y1, //15
	}
}

// tempted to just use the helper function in cube, but eventually will use non cube dimensions for width/depth/height
func makePlayer(x float32, y float32, z float32, width float32, height float32, depth float32, tex *Texture) *Player {
	player := Player{}
	player.x = x
	player.y = y
	player.z = z
	player.width = width
	player.height = height
	player.depth = depth
	player.tex = tex
	player.updateVertices()
	player.indices = []uint32{
		// close face
		0, 2, 1,
		0, 3, 2,
		//far face
		4, 5, 6, //7, 6, 5,
		4, 6, 7, //4, 7, 5,
		//top
		8, 9, 10, //4, 3, 0, //0, 3, 4,
		8, 10, 11, //4, 7, 3, //3, 7, 4,
		//bottom
		14, 13, 12, //5, 1, 2, //1, 2, 5,
		12, 15, 14, //5, 2, 6, //5, 6, 2,
		//right
		4, 1, 5, //0, 1, 5,
		4, 0, 1, //0, 4, 5,
		//left
		7, 6, 2, //3, 7, 2,
		7, 2, 3, //7, 6, 2,
	}

	var ebo uint32
	gl.GenVertexArrays(1, &player.vao)
	gl.BindVertexArray(player.vao)
	gl.GenBuffers(1, &player.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, player.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(player.vertices), gl.Ptr(player.vertices), gl.DYNAMIC_DRAW)
	gl.GenBuffers(1, &ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(player.indices)*4, gl.Ptr(player.indices), gl.STATIC_DRAW)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 5*4, nil)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 5*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)
	return &player
}

func (player *Player) draw() {
	// update vertices to match the current player position
	gl.BindBuffer(gl.ARRAY_BUFFER, player.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(player.vertices), gl.Ptr(player.vertices), gl.DYNAMIC_DRAW)
	// actually draw everything
	gl.BindVertexArray(player.vao)
	gl.DrawElements(gl.TRIANGLES, int32(len(player.indices)), gl.UNSIGNED_INT, nil)
}
