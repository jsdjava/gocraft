package main

import (
	"github.com/go-gl/gl/v4.1-core/gl"
)

type Sun struct {
	x        float32
	y        float32
	z        float32
	width    float32
	height   float32
	depth    float32
	vertices []float32
	indices  []uint32
	vbo      uint32
	vao      uint32
	tex      *Texture
}

// TODO Have a lot of resun.ated code around filling out cube coordinates, should try to DRY this up
func makeSun(x float32, y float32, z float32, width float32, height float32, depth float32, tex *Texture) *Sun {
	sun := Sun{}
	sun.x = x
	sun.y = y
	sun.z = z
	sun.width = width
	sun.height = height
	sun.depth = depth
	sun.tex = tex
	sun.indices = []uint32{
		// close face
		0, 2, 1,
		0, 3, 2,
		//far face
		4, 5, 6, //7, 6, 5,
		4, 6, 7, //4, 7, 5,
		//top
		8, 9, 10, //4, 3, 0, //0, 3, 4,
		8, 10, 11, //4, 7, 3, //3, 7, 4,
		//bottom
		14, 13, 12, //5, 1, 2, //1, 2, 5,
		12, 15, 14, //5, 2, 6, //5, 6, 2,
		//right
		4, 1, 5, //0, 1, 5,
		4, 0, 1, //0, 4, 5,
		//left
		7, 6, 2, //3, 7, 2,
		7, 2, 3, //7, 6, 2,
	}
	// close face
	sun.vertices = []float32{
		sun.x + sun.width, sun.y + sun.height, sun.z + sun.depth, sun.tex.x2, sun.tex.y2, //0
		sun.x + sun.width, sun.y, sun.z + sun.depth, sun.tex.x2, sun.tex.y1, //1
		sun.x, sun.y, sun.z + sun.depth, sun.tex.x1, sun.tex.y1, //2
		sun.x, sun.y + sun.height, sun.z + sun.depth, sun.tex.x1, sun.tex.y2, //3

		// far face
		sun.x + sun.width, sun.y + sun.height, sun.z, sun.tex.x1, sun.tex.y2, //4
		sun.x + sun.width, sun.y, sun.z, sun.tex.x1, sun.tex.y1, //5
		sun.x, sun.y, sun.z, sun.tex.x2, sun.tex.y1, //6
		sun.x, sun.y + sun.height, sun.z, sun.tex.x2, sun.tex.y2, //7

		// eventually, this will sun.obably be a different texture (a lot of the time)
		//tosun.face
		sun.x + sun.width, sun.y + sun.height, sun.z, sun.tex.x2, sun.tex.y2, //8
		sun.x, sun.y + sun.height, sun.z, sun.tex.x1, sun.tex.y2, //9
		sun.x, sun.y + sun.height, sun.z + sun.depth, sun.tex.x1, sun.tex.y1, //10
		sun.x + sun.width, sun.y + sun.height, sun.z + sun.depth, sun.tex.x2, sun.tex.y1, //11

		// bottom face
		sun.x + sun.width, sun.y, sun.z, sun.tex.x2, sun.tex.y2, //12
		sun.x, sun.y, sun.z, sun.tex.x1, sun.tex.y2, //13
		sun.x, sun.y, sun.z + sun.depth, sun.tex.x1, sun.tex.y1, //14
		sun.x + sun.width, sun.y, sun.z + sun.depth, sun.tex.x2, sun.tex.y1, //15
	}

	var ebo uint32
	gl.GenVertexArrays(1, &sun.vao)
	gl.BindVertexArray(sun.vao)
	gl.GenBuffers(1, &sun.vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, sun.vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(sun.vertices), gl.Ptr(sun.vertices), gl.DYNAMIC_DRAW)
	gl.GenBuffers(1, &ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(sun.indices)*4, gl.Ptr(sun.indices), gl.STATIC_DRAW)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 5*4, nil)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 5*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)
	return &sun
}

func (sun *Sun) draw() {
	gl.BindVertexArray(sun.vao)
	gl.DrawElements(gl.TRIANGLES, int32(len(sun.indices)), gl.UNSIGNED_INT, nil)
}
