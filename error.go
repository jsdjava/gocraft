package main

import "log"

// saw this in some example go code, pretty much exactly what i want to use in a bunch of places (quit if err)
func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
