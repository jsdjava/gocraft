package main

import "gocraft/constants"

type Cube struct {
	tex    *Texture
	texTop *Texture
}

// cubeSize, too lazy to write cubeSize all over
var cs float32 = 1 //1

func loadCubes(textures []Texture) []*Cube {
	// will want to be more strategic about this, but for now just going to load up the grass cube
	cubes := make([]*Cube, 20)
	cubes[constants.WATER_CUBE_INDEX] = &Cube{&textures[10*44+18], &textures[10*44+18]}
	cubes[constants.GRASS_CUBE_INDEX] = &Cube{&textures[3*44], &textures[0]}
	cubes[constants.SAND_CUBE_INDEX] = &Cube{&textures[2*44+1], &textures[2*44+1]}
	cubes[constants.WOOD_CUBE_INDEX] = &Cube{&textures[4*44+1], &textures[4*44+1]}
	cubes[constants.STONE_CUBE_INDEX] = &Cube{&textures[1*44], &textures[1*44]}
	cubes[constants.DIRT_CUBE_INDEX] = &Cube{&textures[2*44], &textures[2*44]}
	cubes[constants.BEDROCK_CUBE_INDEX] = &Cube{&textures[1*44+1], &textures[1*44+1]}
	cubes[constants.GOLD_CUBE_INDEX] = &Cube{&textures[2], &textures[2]}
	cubes[constants.IRON_CUBE_INDEX] = &Cube{&textures[1*44+2], &textures[1*44+2]}
	cubes[constants.DIAMOND_CUBE_INDEX] = &Cube{&textures[2*44+3], &textures[2*44+3]}
	cubes[constants.REDSTONE_CUBE_INDEX] = &Cube{&textures[3*44+3], &textures[3*44+3]}
	cubes[constants.LAPIS_CUBE_INDEX] = &Cube{&textures[10], &textures[10]}
	cubes[constants.COAL_CUBE_INDEX] = &Cube{&textures[2*44+2], &textures[2*44+2]}
	return cubes
}

func getCubeIndicesAndVertices(
	x float32,
	y float32,
	z float32,
	filled bool,
	indices []uint32,
	vertices []float32,
	left bool,
	right bool,
	below bool,
	above bool,
	// straight up guessing, not quite sure if these are reversed
	back bool,
	front bool,
	c *Cube,
) ([]uint32, []float32) {
	//renderer can't handle nothing, so getting unlucky and not seeing a cube.
	// TODO Make renderer ok if somehow theres no data to draw
	// i am dumb, totally forgot to not render the actual cube when its not filled
	// thought of a little optimization, if a cube is fully contained we can just quit out as well
	var fullyContained = left && right && above && below && back && front
	if !filled || fullyContained {
		return indices, vertices
	}
	var initialNumIndices = len(indices)
	var numVertices = len(vertices)
	// i think i need to change the texture coords for pretty much every face in the cube
	// meaning we're going to need more vertices aggg
	// looks like i can share 4 faces by just flipping textures coords on the far face, and
	// the top had to be unique anyway, so not a big deal
	vertices = append(
		vertices,
		// close face
		x+cs, y+cs, z+cs, c.tex.x2, c.tex.y2, //0
		x+cs, y, z+cs, c.tex.x2, c.tex.y1, //1
		x, y, z+cs, c.tex.x1, c.tex.y1, //2
		x, y+cs, z+cs, c.tex.x1, c.tex.y2, //3

		// far face
		x+cs, y+cs, z, c.tex.x1, c.tex.y2, //4
		x+cs, y, z, c.tex.x1, c.tex.y1, //5
		x, y, z, c.tex.x2, c.tex.y1, //6
		x, y+cs, z, c.tex.x2, c.tex.y2, //7

		// eventually, this will probably be a different texture (a lot of the time)
		//top face
		x+cs, y+cs, z, c.texTop.x2, c.texTop.y2, //8
		x, y+cs, z, c.texTop.x1, c.texTop.y2, //9
		x, y+cs, z+cs, c.texTop.x1, c.texTop.y1, //10
		x+cs, y+cs, z+cs, c.texTop.x2, c.texTop.y1, //11

		// bottom face
		x+cs, y, z, c.tex.x2, c.tex.y2, //12
		x, y, z, c.tex.x1, c.tex.y2, //13
		x, y, z+cs, c.tex.x1, c.tex.y1, //14
		x+cs, y, z+cs, c.tex.x2, c.tex.y1, //15
	)
	// easy test to see if its backface culling right
	// why is it below and not above???? i setup the coords up
	// so above would have the CCW front facing
	// nvm, had a dumb model matrix being applied, should be fixed now
	// something like these 3 faces, might have one wrong...
	//front = true
	// top = true
	// right = true
	// believe culling is still working right (top cover bottom, bottom cover top)
	if !front {
		indices = append(
			indices,
			// close face
			0, 2, 1, //3, 2, 1,
			0, 3, 2, //0, 3, 1,
		)
	}
	if !back {
		indices = append(
			indices,
			//far face
			4, 5, 6, //7, 6, 5,
			4, 6, 7, //4, 7, 5,
		)
	}
	if !above {
		indices = append(
			indices,
			//top
			8, 9, 10, //4, 3, 0, //0, 3, 4,
			8, 10, 11, //4, 7, 3, //3, 7, 4,
		)
	}
	//legitimately screwed up an index here, creating an X aaaaaaaaaaaaaaaaaaahhhhhhh
	if !below {
		indices = append(
			indices,
			//bottom
			14, 13, 12, //5, 1, 2, //1, 2, 5,
			12, 15, 14, //5, 2, 6, //5, 6, 2,
		)
	}
	if !right {
		indices = append(
			indices,
			//right
			4, 1, 5, //0, 1, 5,
			4, 0, 1, //0, 4, 5,
		)
	}
	if !left {
		indices = append(
			indices,
			//left
			7, 6, 2, //3, 7, 2,
			7, 2, 3, //7, 6, 2,
		)
	}
	// AHG its offset by the number of vertices, not by the number of indices
	for i := initialNumIndices; i < len(indices); i++ {
		/// its this, its this divide by 3 AHHHHHHHHHHHHHHHHHHHHHH
		// it should be divide by 5, because "numVertices" is actually the size of the vertex [], which now
		// has 5 elements HHHHHHHHHHHHHHHHHHHASDOIFHASIDFH
		indices[i] += uint32(numVertices / 5)
	}
	return indices, vertices
}
