package main

import "github.com/go-gl/gl/v4.1-core/gl"

var skyVertexShaderStr = `
#version 410 core

layout (location = 0) in vec3 pos;
out vec4 vertexColor;

const vec3 topColor = vec3(0.529,0.808,0.980);
const vec3 botColor = vec3(0,0.749,1);

void main()
{
	gl_Position = vec4(pos,1.0);
	vertexColor = vec4(vec3(topColor*(1-pos.y)+botColor*pos.y),1);
}
` + "\x00"

var skyFragmentShaderStr = `
#version 410 core
out vec4 FragColor;

in vec4 vertexColor;
void main()
{
	FragColor = vertexColor;
}
` + "\x00"

var skyProg uint32

func initSkyShaders() {
	skyVertexShader, err := compileShader(skyVertexShaderStr, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}
	skyFragmentShader, err := compileShader(skyFragmentShaderStr, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}
	skyProg = gl.CreateProgram()
	gl.AttachShader(skyProg, skyVertexShader)
	gl.AttachShader(skyProg, skyFragmentShader)
	gl.LinkProgram(skyProg)
	gl.DeleteShader(skyVertexShader)
	gl.DeleteShader(skyFragmentShader)
}

var skyVao uint32

func makeSky() {
	// surely theres a better way to do this lmao
	vertices := []float32{
		-1, -1, 0.99999,
		-1, 1, 0.99999,
		1, 1, 0.99999,
		1, -1, 0.99999,
	}
	indices := []uint32{
		2, 1, 0,
		2, 0, 3,
	}
	var vbo uint32
	var ebo uint32
	gl.GenVertexArrays(1, &skyVao)
	gl.BindVertexArray(skyVao)
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	// an hour of my life gone because i didn't realize i was passing the actual vbo instead of an enum
	// for what type it was (bindbuffer already set it up in the opengl state machine)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertices)*4, gl.Ptr(vertices), gl.STATIC_DRAW)
	gl.GenBuffers(1, &ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indices)*4, gl.Ptr(indices), gl.STATIC_DRAW)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 3*4, nil)
	gl.EnableVertexAttribArray(0)
}

func drawSky() {
	gl.UseProgram(skyProg)
	gl.BindVertexArray(skyVao)
	gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, nil)
}
