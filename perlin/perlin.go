package perlin

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/go-gl/mathgl/mgl32"
)

// note, i will need to use 3d perlin noise to get overhangs and caves, lets solve for 2d case for now
// generate a 2D grid of "random" gradient vectors - pick randomly from the ordinal/cardinal unit vectors
// foreach pixel, figure out where it is in the grid (4 nearest grid points). compute the vectors to those
// 4 points, then calculate the dot products of each of those vectors with the random ordinal/cardinal vectors
// for those points. these 4 values are successively lerp-ed to get a final single value for the pixel

// need to move this to 3 dimensions and think through how the infinite load works with it
// first, need to add octaves

// nvm, to do proper "scaling" its easiest to use floats anyway

// we don't need full vectors objects/dot products, the possible vectors make it really easy to just statically define the dot product results
// dot product is defined as ax*bx + ay*by
func dot(index int, x float32, y float32) float32 {
	switch index {
	case 0: //{1, 0}
		return x
	case 1: //{1, 1}
		return x + y
	case 2: //{0, 1}
		return y
	case 3: //{-1, 1},
		return y - x
	case 4: //{-1, 0}
		return -x
	case 5: //{-1, -1}
		return -(y + x)
	case 6: //{0, -1}
		return -y
	case 7: //{1, -1}
		return x - y
	}
	// should be unreachable
	return 0
}

// sick of writing this everywhere
func abs(v int) int {
	return int(math.Abs(float64(v)))
}

func ChunkHash(seed uint, x int, y int) uint {
	// map from signed to unsigned by making negatives be odds and positives be evens
	if x >= 0 {
		x = x * 2
	} else {
		x = x*2 - 1
	}
	if y >= 0 {
		y = y * 2
	} else {
		y = y*2 - 1
	}
	// use "cantor" mapping - https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
	ax := abs(x)
	ay := abs(y)
	return uint((ax+ay)*(ax+ay+1)+ay)/2 ^ seed
}

type PerlinList struct {
	seed  uint
	rands []float32
}

// so, perlin caves ("worms") i need to generate completely
// as opposed to on the fly based on what chunk im in, meaning when i make a perlin
// list i can just fill it at the same time
// i need to make a list thats longer than length, because i have to satisfy max number of octaves out,
// so if i do 3 octaves, then i need length + 2^2 values * 2 (negative and positive) out
func MakeList(seed uint, length uint, numOctaves uint) *PerlinList {
	randsLength := length + 1 + uint(math.Pow(2, float64(numOctaves)-1))*2
	g := PerlinList{seed, make([]float32, randsLength)}
	listRand := rand.New(rand.NewSource(int64(seed)))
	for i := 0; i < int(randsLength); i++ {
		g.rands[i] = listRand.Float32()
	}
	return &g
}

type PerlinGrid struct {
	size        int
	octaves     int
	seed        uint
	min         int
	max         int
	chunks      map[uint][][]byte
	pixels      map[uint]byte
	chunksMutex sync.RWMutex
	pixelsMutex sync.RWMutex
}

func MakeGrid(size int, octaves int, seed uint, min int, max int) *PerlinGrid {
	g := PerlinGrid{size, octaves, seed, min, max, make(map[uint][][]byte), make(map[uint]byte), sync.RWMutex{}, sync.RWMutex{}}
	return &g
}

func getChunkRands(gridSize int, seed uint, x int, y int) [][]byte {
	//return gridSize-1,gridSize-1
	rands := make([][]byte, gridSize-1)
	chunkSeed := ChunkHash(seed, x, y)
	chunkRand := rand.New(rand.NewSource(int64(chunkSeed)))
	for i := 0; i < gridSize-1; i++ {
		rands[i] = make([]byte, gridSize-1)
		for j := 0; j < gridSize-1; j++ {
			rands[i][j] = byte(chunkRand.Intn(8))
		}
	}
	return rands
}

// really, the way i want to consume this function is just asking it to give
// me more noise when I need it (and where I need it)
// but for now, lets just add octaves, then solve that problem
// gridsize no longer defines when perlin repeats at the lowest octave, it now defines when
// it repeats at the HIGHEST octave
func (grid *PerlinGrid) getGridValue(x int, y int) byte {
	// of course its not that simple. x/y aren't just modulus-ed, they are mirrored when negative
	xIndex := x % grid.size
	yIndex := y % grid.size
	if xIndex < 0 {
		xIndex = grid.size + xIndex - 1
	}
	if yIndex < 0 {
		yIndex = grid.size + yIndex - 1
	}
	// we can probably just hold the grid (completely?) in memory for now as we walk around
	// we have to remember, though, that random chunks can access the grid in a non-deterministic order,
	// so either need to lock around this action, or need to pregenerate the necessary grid pieces. scratch that,
	// people just generate seeds per chunk (using a hash of the chunk coords and the initial seed).
	// note, we weirdly need to still "share" a little bit because neighbor chunks need to give us their edges
	//  _ _ _ _
	// |       |
	// |       |
	// |       |
	// |_ _ _ _|
	// generate rands for x,y-1
	chunkX := x / grid.size
	chunkY := y / grid.size
	key := ChunkHash(1, chunkX, chunkY)
	grid.chunksMutex.RLock()
	if v, ok := grid.chunks[key]; ok {
		grid.chunksMutex.RUnlock()
		return v[xIndex][yIndex]
	}
	grid.chunksMutex.RUnlock()
	bottomRands := getChunkRands(grid.size, grid.seed, chunkX, chunkY-1)
	rightRands := getChunkRands(grid.size, grid.seed, chunkX+1, chunkY)
	chunkRands := getChunkRands(grid.size, grid.seed, chunkX, chunkY)
	bottomRightRands := getChunkRands(grid.size, grid.seed, chunkX+1, chunkY-1)
	// need 2 points at least to interpolate between them
	rands := make([][]byte, grid.size)
	for i := grid.size - 1; i > 0; i-- {
		rands[i] = make([]byte, grid.size)
		for j := grid.size - 1; j > 0; j-- {
			rands[i][j] = chunkRands[i-1][j-1]
		}
	}
	rands[0] = make([]byte, grid.size)
	for i := 0; i < grid.size-1; i++ {
		rands[0][i] = bottomRands[grid.size-2][i]
	}
	for j := grid.size - 2; j >= 0; j-- {
		rands[j][grid.size-1] = rightRands[j][0]
	}
	rands[0][grid.size-1] = bottomRightRands[grid.size-2][0]
	grid.chunksMutex.Lock()
	grid.chunks[key] = rands
	grid.chunksMutex.Unlock()
	return rands[xIndex][yIndex]
}

// lets notmemoize the fade fn
// maybe its literally just the smoothness function that fixes what happens at the edges of each grid rn
func lerp(v1 float32, v2 float32, weight float32) float32 {
	w := weight * weight * weight * (6*weight*weight - 15*weight + 10)
	return v1*w + (1-w)*v2
}

// so big screwup here was constantly confusing myself on which direction to lerp, and also
// completely, fundamentally misunderstanding octaves - you don't generate a new perlin noise
// grid for each one, octaves just mean you pick from successively less fine positions in the grid
// otherwise, continuity is lost, especially when the big grid ends up totally dominating
// i also think if u have too many octaves, details disappear at a fine level because the big one
// just wins out, but this is still a little confusing/shaky to me
// i don't think? it matters if coordinates coming in here are cartesian (or reversed in the y),
// but note the grid is not cartesian(in how we index it). if you flipped the grid though
// i think it would be the same result so should be fine
// i thinkkkkkkkk the result is scaled from -1 to 1 -> not quite, its actually from +/-1/sqrt(1/N)

// this is really extremely inefficient, lets come up with some ways to fix it
// it feels like we're doing a lot of unnecessary division in here...
var minHeight = -1
var maxHeight = -1

func (grid *PerlinGrid) GetCachedPixel(x int, y int, ppg int) byte {
	// seed doesn't matter, we just want to efficiently hash x and y
	key := ChunkHash(0, x, y)
	grid.pixelsMutex.RLock()
	if p, ok := grid.pixels[key]; ok {
		grid.pixelsMutex.RUnlock()
		return p
	}
	grid.pixelsMutex.RUnlock()
	p := grid.GetPixel(x, y, ppg)
	grid.pixelsMutex.Lock()
	grid.pixels[key] = p
	grid.pixelsMutex.Unlock()
	return p
}

func (grid *PerlinGrid) GetPixel(x int, y int, ppg int) byte {
	perlinNoise := float32(0)
	perlinHeight := float32(0)
	curHeight := float32(1)
	inverseNoiseFrequency := 1
	for k := 0; k < grid.octaves; k++ {
		//1,2,4,8...2^n
		ppo := ppg * inverseNoiseFrequency
		xGrid := x / ppo * ppo
		yGrid := y / ppo * ppo
		// none of these are quite right anymore because the grid is infinite in up/down and left/right directions
		i1 := (x / ppo) * inverseNoiseFrequency
		j1 := (y / ppo) * inverseNoiseFrequency
		i2 := i1 + inverseNoiseFrequency
		j2 := j1 + inverseNoiseFrequency
		if x < 0 {
			i2 = i1 - inverseNoiseFrequency
		}
		if y < 0 {
			j2 = j1 - inverseNoiseFrequency
		}
		fppo := float32(ppo)
		gvi1 := grid.getGridValue(i1, j1)
		gvi2 := grid.getGridValue(i2, j1)
		gvi3 := grid.getGridValue(i2, j2)
		gvi4 := grid.getGridValue(i1, j2)

		// welp, this worked. i feel like this was way too complicated.
		// could i just use cantor mapping to calculate everything in the perlin noise function???
		axGrid := abs(xGrid)
		ayGrid := abs(yGrid)
		xGridMore := axGrid + ppo
		yGridMore := ayGrid + ppo
		ax := abs(x)
		ay := abs(y)

		//fmt.Printf("%d,%d,%d,%d,%d,%d,%d\n", k, x, y, i1, j1, i2, j2)
		// pretty confident theres some optimization we can do here, likely not needed
		dp1 := dot(int(gvi1), float32(ax-axGrid)/fppo, float32(ay-ayGrid)/fppo)
		dp2 := dot(int(gvi2), float32(ax-(xGridMore))/fppo, float32(ay-ayGrid)/fppo)
		dp3 := dot(int(gvi3), float32(ax-(xGridMore))/fppo, float32(ay-(yGridMore))/fppo)
		dp4 := dot(int(gvi4), float32(ax-axGrid)/fppo, float32(ay-(yGridMore))/fppo)
		//fmt.Printf("%d,%d,%d,%f,%f,%f,%f\n", k, x, y, dp1, dp2, dp3, dp4)
		// should range from -2 to 2
		//fmt.Printf("%f,%f,%f,%f\n", dp1, dp2, dp3, dp4)
		// interpolation is dp1 to dp2 on the x axis and dp3 to dp4 on the x axis
		// then the last two values are interpolated on the y axis
		// ordering obviously matter when lerp-ing (otw u spend 2 hours looking at grids where the noise isn't smooth
		// because you reversed top/bottom
		li1 := lerp(dp1, dp2, float32(xGridMore-ax)/fppo)
		li2 := lerp(dp4, dp3, float32(xGridMore-ax)/fppo)
		// one thing thats clear is using a a height based off 2^n is a bad idea because it makes the final octave(s) wayyyyyy too
		// powerful - lets tone height down
		// 1, 1.1, 1.21, 1.331, etc
		// it feels like as i move to upper octaves, the perlinnoise being outputted is NOT scaled from -1 to 1, but i don't understand why...
		perlinNoise += (lerp(li1, li2, float32(yGridMore-ay)/fppo) * curHeight)
		//fmt.Printf("%f,%f,%f\n", perlinNoise, dp1, dp2)
		perlinHeight += curHeight
		curHeight *= 2
		inverseNoiseFrequency = inverseNoiseFrequency << 1
	}
	//fmt.Printf("%f,%f\n", perlinNoise, perlinHeight)
	ret := byte(((perlinNoise/perlinHeight + 0.707) / 1.414 * float32(grid.max-grid.min)) + float32(grid.min))
	/*if ret < byte(minHeight) || minHeight == -1 {
		minHeight = int(ret)
		fmt.Printf("min height: %d\n", minHeight)
	}
	if ret > byte(maxHeight) || maxHeight == -1 {
		maxHeight = int(ret)
		fmt.Printf("max height: %d\n", maxHeight)
	}*/
	return ret
}

const gridSize = 4 //4

// 16*16 * 32
const imgSize = 90 //300

/*
	perlinNoise := float32(0)
	perlinHeight := float32(0)
	curHeight := float32(1)
	inverseNoiseFrequency := 1
*/
// smarter strat - figure out what chunk we need in getPixel (div by gridsPerChunk, % by gridsPerChunk)
// store a hashmap of chunks - have a fn called getChunkRand, takes in chunkx,chunky,x,y
// if in hashmap returns x,y in it, otw seeds rand generator, generates the chunk, stores it, and returns the x,y in it
func perlinTest() {
	grid := MakeGrid(gridSize, 6, 29, 0, 255)
	img := image.NewRGBA(image.Rectangle{image.Pt(0, 0), image.Pt(imgSize*2+1, imgSize*2+1)})
	start := time.Now()
	// this is running quite a bit slower than i thought it would, so need to investigate
	// otoh, i only need to generate probably 300 chunks quickly, so lets see how long that takes
	for x := -imgSize; x <= imgSize; x++ {
		for y := -imgSize; y <= imgSize; y++ {
			//fmt.Printf("%d,%d\n", x, y) //
			perlinPixel := grid.GetPixel(x, y, 4) //grid.GetPixelV2(x, y, 6, 256)
			img.Set(x+imgSize, y+imgSize, color.RGBA{perlinPixel, perlinPixel, perlinPixel, 0xff})
		}
	}
	elapsed := time.Since(start)
	fmt.Print(elapsed)
	f, _ := os.Create("perlin.png")
	png.Encode(f, img)
}

func (list *PerlinList) Perlin1D(ppv uint, min int, max int, numOctaves uint) []float32 {
	offset := uint(math.Pow(2, float64(numOctaves)-1)) * 2
	length := (uint(len(list.rands)) - offset*2) * ppv
	perlinNoises := make([]float32, length)
	for i := offset; i < length+offset; i++ {
		perlinNoise := float32(0)
		perlinHeight := float32(0)
		curHeight := uint(1)
		for k := uint(0); k < numOctaves; k++ {
			perlinIndex := i / ppv
			perlinLeftIndex := (perlinIndex / curHeight) * curHeight
			perlinRightIndex := (perlinIndex/curHeight + 1) * curHeight
			lv1 := list.rands[perlinLeftIndex]
			lv2 := list.rands[perlinRightIndex]
			perlinNoise += lerp(lv2, lv1, float32(i%(ppv*curHeight))/float32(ppv*curHeight)) * float32(curHeight)
			perlinHeight += float32(curHeight)
			curHeight *= 2
		}
		perlinNoises[i-offset] = (perlinNoise/perlinHeight)*float32(max-min) + float32(min)
	}
	return perlinNoises
}

type Point struct {
	x float64
	y float64
}

func drawLine(p1 Point, p2 Point, img *image.RGBA) {
	if p1.x < p2.x {
		slope := (p2.y - p1.y) / (p2.x - p1.x)
		for x := 0; x <= int(p2.x-p1.x); x++ {
			y := slope * float64(x)
			img.Set(int(p1.x)+x, int(p1.y+y), color.White)
		}
	} else if p1.x > p2.x {
		slope := (p2.y - p1.y) / (p2.x - p1.x)
		for x := 0; x <= int(p1.x-p2.x); x++ {
			y := slope * float64(x)
			img.Set(int(p2.x)+x, int(p2.y+y), color.White)
		}
	}
	if p1.y < p2.y {
		slope := (p2.x - p1.x) / (p2.y - p1.y)
		for y := 0; y <= int(p2.y-p1.y); y++ {
			x := slope * float64(y)
			img.Set(int(p1.x+x), int(p1.y)+y, color.White)
		}
	} else if p1.y > p2.y {
		slope := (p2.x - p1.x) / (p2.y - p1.y)
		for y := 0; y <= int(p1.y-p2.y); y++ {
			x := slope * float64(y)
			img.Set(int(p2.x+x), int(p2.y)+y, color.White)
		}
	}
}

func drawCircle(x int, y int, r int, img *image.RGBA) {
	rSquared := r * r
	for i := x - r; i <= x+r; i++ {
		for j := y - r; j < y+r; j++ {
			// distance formula dude
			if (x-i)*(x-i)+(y-j)*(y-j) <= rSquared {
				img.Set(i, j, color.RGBA{0, 255, 0, 255})
			}
		}
	}
}

// So i gave this some thought, and did a little research, and i think that we need to switch
// to an algorithm that generates a delta in the x, y, and z as well as a thickness. This has the
// advantage of being much quicker computationally, and also a lot easier to reason about
func perlinWormV2Test() {
	img := image.NewRGBA(image.Rectangle{image.Pt(0, 0), image.Pt(1000, 1000)})
	for x := 0; x < 1000; x++ {
		for y := 0; y < 1000; y++ {
			img.Set(x, y, color.RGBA{0, 0, 0, 0xff})
		}
	}
	start := time.Now()
	ppv := uint(8)
	wormLength := uint(65)
	radiusList := MakeList(67, wormLength, 3)
	radiusPerlin := radiusList.Perlin1D(ppv, 4, 10, 3)
	xList := MakeList(32, wormLength, 3)
	xPerlin := xList.Perlin1D(ppv, -8, 8, 3)
	yList := MakeList(89, wormLength, 3)
	yPerlin := yList.Perlin1D(ppv, -8, 8, 3)
	x := float32(500.0)
	y := float32(500.0)
	for i := 0; i < len(radiusPerlin); i++ {
		x += xPerlin[i]
		y += yPerlin[i]
		drawCircle(int(x), int(y), int(radiusPerlin[i]), img)
		img.Set(int(x), int(y), color.RGBA{255, 0, 0, 255})
	}
	elapsed := time.Since(start)
	fmt.Print(elapsed)
	f, _ := os.Create("perlinWorm.png")
	png.Encode(f, img)
}

func perlinWormTest() {
	img := image.NewRGBA(image.Rectangle{image.Pt(0, 0), image.Pt(1000, 1000)})
	for x := 0; x < 1000; x++ {
		for y := 0; y < 1000; y++ {
			img.Set(x, y, color.RGBA{0, 0, 0, 0xff})
		}
	}
	start := time.Now()
	wormLength := uint(50)
	thicknessList := MakeList(23, wormLength, 1)
	thicknessPerlin := thicknessList.Perlin1D(20, 5, 10, 1)
	angleList := MakeList(19, wormLength, 1)
	anglePerlin := angleList.Perlin1D(20, -180, 180, 1)
	//fmt.Print(thicknessPerlin)
	//fmt.Print(anglePerlin)
	prevP1 := Point{500, 500}
	prevP2 := Point{}
	c := 10.0
	for i := 0; i < len(anglePerlin); i++ {
		angle := anglePerlin[i]
		thickness := thicknessPerlin[i]
		// remember, its the normal that we want to move along, so
		// we reverse x and y OR NOT THAT WOULD BE STUPID,
		// what we should do is add 90 degrees
		curP1 := Point{
			prevP1.x + c*math.Sin(float64(mgl32.DegToRad(float32(angle)))),
			prevP1.y + c*math.Cos(float64(mgl32.DegToRad(float32(angle)))),
		}
		curP2 := Point{
			float64(thickness)*math.Sin(float64(mgl32.DegToRad(float32(angle-90)))) + float64(curP1.x),
			float64(thickness)*math.Cos(float64(mgl32.DegToRad(float32(angle-90)))) + float64(curP1.y),
		}
		if i == 0 {
			prevP1 = curP1
			prevP2 = curP2
			continue
		}
		drawLine(prevP1, prevP2, img)
		img.Set(int(prevP1.x), int(prevP1.y), color.RGBA{0, 255, 0, 255})
		img.Set(int(prevP2.x), int(prevP2.y), color.RGBA{255, 0, 0, 255})
		prevP1 = curP1
		prevP2 = curP2
	}

	elapsed := time.Since(start)
	fmt.Print(elapsed)
	f, _ := os.Create("perlinWorm.png")
	png.Encode(f, img)
}

func main() {
	perlinTest()
}
