# Gocraft
## Demo

[![Thumbnail](pics/gocraft.gif)](https://drive.google.com/file/d/1yIqkaMe8CnZsuuMEXDsHyE0D5R06PEsu/view?usp=sharing "Assembly Pong - Click to open video")
[Click image (or here) to open video](https://drive.google.com/file/d/1yIqkaMe8CnZsuuMEXDsHyE0D5R06PEsu/view?usp=sharing)

## Description
Gocraft is a project I wrote to learn both OpenGL and golang by making a minecraft clone.
The comment style and branches are a little bit weird because I'm considering making a video showing progress
and lessons learned, so I used the comments to give myself notes about what was going on at the time.

Major features completed are:
* 3D Cube + Chunk rendering engine
* Movement
* Intersection/Physics
* Block creation and destruction
* Infinite Terrain generation using 2D Perlin Noise
* Infinite Biome generation
* Cave generation using Perlin Noise
* World saving/loading

Other features I may implement in the future are:
* Trees
* Water physics
* Make block changes actually save
* Inventory

## Running Locally
` go run .`

## Attribution
Textures were taken from https://www.nicepng.com/maxp/u2w7e6w7i1i1i1i1/
