package main

import (
	"bufio"
	"fmt"
	"gocraft/biomes"
	"gocraft/perlin"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

// kind of annoying that vars cross between files tbh
const chunkSize int = 16    //16
const chunkHeight int = 256 //256
const numCubes = chunkSize * chunkSize * chunkHeight

// Its definitely true that creating a vao/vbo per chunk is smarter than per cube,
// so we did  explore that option eventually
type Chunk struct {
	X int
	Y int
	Z int
	// i am going to serialize this, but I'm just going to write a custom
	// format to do it, can introduce some tricks then if i want
	// probably best to combine these into one cube concept
	Filled      []byte
	CubeIndices []byte
	// turns out serialization is not cheap - i think this makes sense, reading/writing from
	// disk is slow, so we need to just recompute most stuff
	key      string
	changed  bool
	dirty    bool
	indices  []uint32
	vertices []float32
	bounds   [8]mgl32.Vec3 // used to determine if we are in the frustrum or not
	// dont serialize this, its just a ptr to our opengl vertex array object holding all our cubes' triangles
	vao uint32
	vbo uint32
	ebo uint32
}

// TODO I think i should be using pointers to my struct when i want to set things...
//https://stackoverflow.com/questions/28020070/golang-serialize-and-deserialize-back/30721381
func (chunk *Chunk) save(fileName string) {
	//fmt.Printf("Saving %s", fileName)
	// write out filleds, write out texture indices
	// i dont think i need all 256 spots on the texture that i have
	// so technically could probably use a texture index as a "not filled"
	// state, but im just going to keep it simple for now
	if !chunk.changed {
		return
	}
	//fmt.Printf("Really saving %s", fileName)
	f, err := os.Create(fileName)
	check(err)
	defer f.Close()
	w := bufio.NewWriter(f)
	_, err = w.Write(chunk.Filled)
	check(err)
	_, err = w.Write(chunk.CubeIndices)
	check(err)
	w.Flush()
}

func load(filename string) *Chunk {
	c := Chunk{}
	f, err := os.Open(filename)
	check(err)
	defer f.Close()
	c.Filled = make([]byte, numCubes)
	c.CubeIndices = make([]byte, numCubes)
	_, err = io.ReadAtLeast(f, c.Filled, numCubes)
	check(err)
	_, err = io.ReadAtLeast(f, c.CubeIndices, numCubes)
	check(err)
	return &c
}

func (chunk *Chunk) inFrustrum(planes []Plane) bool {
	for p := 0; p < len(planes); p++ {
		var in = 0
		for c := 0; c < len(chunk.bounds) && in == 0; c++ {
			var sign = planes[p].getSignedDistance(chunk.bounds[c])
			// completely guessing here on the sign, just try reversing it if nothing renders
			if sign > 0 {
				in++
			}
		}
		// if all corners are completely outside any plane, we know its
		// not in the frustrum
		if in == 0 {
			return false
		}
	}
	return true
}

func getBounds(chunkX int, chunkY int, chunkZ int) [8]mgl32.Vec3 {
	return [8]mgl32.Vec3{
		{float32(chunkX*chunkSize) * cs, float32(chunkY*chunkHeight) * cs, float32(chunkZ*chunkSize) * cs},
		{float32(chunkX*chunkSize+chunkSize) * cs, float32(chunkY*chunkHeight) * cs, float32(chunkZ*chunkSize) * cs},
		{float32(chunkX*chunkSize+chunkSize) * cs, float32(chunkY*chunkHeight+chunkHeight) * cs, float32(chunkZ*chunkSize) * cs},
		{float32(chunkX*chunkSize) * cs, float32(chunkY*chunkHeight+chunkHeight) * cs, float32(chunkZ*chunkSize) * cs},

		{float32(chunkX*chunkSize) * cs, float32(chunkY*chunkHeight) * cs, float32(chunkZ*chunkSize+chunkSize) * cs},
		{float32(chunkX*chunkSize+chunkSize) * cs, float32(chunkY*chunkHeight) * cs, float32(chunkZ*chunkSize+chunkSize) * cs},
		{float32(chunkX*chunkSize+chunkSize) * cs, float32(chunkY*chunkHeight+chunkHeight) * cs, float32(chunkZ*chunkSize+chunkSize) * cs},
		{float32(chunkX*chunkSize) * cs, float32(chunkY*chunkHeight+chunkHeight) * cs, float32(chunkZ*chunkSize+chunkSize) * cs},
	}
}
func min(v1 int, v2 int) int {
	if v1 < v2 {
		return v1
	}
	return v2
}

// stupid golang math.max is float64, just coding my own w/e
func max(v1 int, v2 int) int {
	if v1 > v2 {
		return v1
	}
	return v2
}

func (chunk *Chunk) tryBuildCube(xIndex int, zIndex int, y int) {
	var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + y
	chunk.Filled[index] = 1
}

func (chunk *Chunk) undoBuildCube(xIndex int, zIndex int, y int) {
	var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + y
	chunk.Filled[index] = 0
}

func (chunk *Chunk) destroyCube(xIndex int, zIndex int, y int) {
	var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + y
	chunk.Filled[index] = 0
	chunk.updateChunk()
}

// intended to be called before we load into opengl
func (chunk *Chunk) erodeCube(xIndex int, zIndex int, y int) {
	var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + y
	chunk.Filled[index] = 0
}

func (chunk *Chunk) filled(xIndex int, zIndex int, y int) bool {
	var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + y
	return chunk.Filled[index] > 0
}

func (chunk *Chunk) intersecting(xIndex int, zIndex int, minY int, maxY int) bool {
	for k := minY; k < maxY; k++ {
		var index = xIndex*chunkSize*chunkHeight + zIndex*chunkHeight + k
		if chunk.Filled[index] > 0 {
			return true
		}
	}
	return false
}

/*func (chunk *Chunk) intersecting(x1 int, x2 int, z1 int, z2 int, minY int, maxY int, cameraX float32, cameraZ float32) bool {
	minX := min(x1, x2)
	maxX := max(x1, x2)
	minZ := min(z1, z2)
	maxZ := max(z1, z2)
	if math.Abs(float64(maxX-minX)) > 2 {
		fmt.Printf("problem")
	}
	if math.Abs(float64(maxZ-minZ)) > 2 {
		fmt.Printf("problem")
	}
	var chunkX, chunkZ int
	fmt.Sscanf(chunk.key, "%d,%d", &chunkX, &chunkZ)
	for i := minX; i <= maxX; i++ {
		for j := minZ; j < maxZ; j++ {
			for k := minY; k <= maxY; k++ {
				var index = i*chunkSize*chunkHeight + j*chunkHeight + k
				fmt.Printf("%d,%d - %f,%f\n", chunkX*chunkSize+i, chunkZ*chunkSize+j, cameraX, cameraZ)
				if chunk.Filled[index] > 0 {
					return true
				}
			}
		}
	}
	return false
}*/

// extremely inefficient, curious to see how it performs (im guessing badly)
func (chunk *Chunk) updateChunk() {
	//var updateStartTime = time.Now()
	chunk.changed = true
	chunk.vertices = chunk.vertices[:0]
	chunk.indices = chunk.indices[:0]
	for x := 0; x < chunkSize; x++ {
		for z := 0; z < chunkSize; z++ {
			for y := 0; y < chunkHeight; y++ {
				// looks like disabling this causes more of a steady state (with gc occurring) at 107/108kb
				// maybe not? although it takes a lot longer, we eventually reach a steady state of 893MB?
				// 15X15Xsizeof(chunk)
				// sizeof(chunk) = 65536 + 65536 + 36*65536 + 4*80*65536 ~=40mb
				// no, i did this computation wrong
				// 255*(65536 + 65536 + 36*65536 + 4*80*65536) = 6GB
				// using the logic that 1 in 20 cubes is filled (on avg), we get pretty close to 900MB
				index := x*chunkSize*chunkHeight + z*chunkHeight + y
				leftIndex := (x-1)*chunkSize*chunkHeight + z*chunkHeight + y
				rightIndex := (x+1)*chunkSize*chunkHeight + z*chunkHeight + y
				belowIndex := x*chunkSize*chunkHeight + z*chunkHeight + y - 1
				aboveIndex := x*chunkSize*chunkHeight + z*chunkHeight + y + 1
				backIndex := x*chunkSize*chunkHeight + (z+1)*chunkHeight + y
				frontIndex := x*chunkSize*chunkHeight + (z-1)*chunkHeight + y
				// this function is way too big (12 arguments!!!), good indication
				// we need to rethink the architecture a little
				//6 neighbors, above,below,left,right,back,front
				chunk.indices, chunk.vertices = getCubeIndicesAndVertices(
					float32(x+chunk.X*chunkSize),
					float32(y+chunk.Y*chunkHeight),
					float32(z+chunk.Z*chunkSize),
					chunk.Filled[index] > 0,
					chunk.indices,
					chunk.vertices,
					x > 0 && chunk.Filled[leftIndex] > 0,
					x < chunkSize-1 && chunk.Filled[rightIndex] > 0,
					y > 0 && chunk.Filled[belowIndex] > 0,
					y < chunkHeight-1 && chunk.Filled[aboveIndex] > 0,
					// caught a small bug here where these were reversed
					z > 0 && chunk.Filled[frontIndex] > 0,
					z < chunkSize-1 && chunk.Filled[backIndex] > 0,
					cubes[chunk.CubeIndices[index]],
				)
			}
		}
	}
	chunk.dirty = true
	//fmt.Printf("updateChunk() elapsedTime: %d\n", time.Since(updateStartTime)/1000000)
}

const WATER_LEVEL = 63

// ok, lets do a little refactoring
func makeChunk(
	biomes *biomes.Biomes,
	chunkX int, chunkY int, chunkZ int,
	cubes []*Cube,
	cubeLayersSeed int,
	blendSeed int,
	cubeTypeSeed int,
	cubeHeightSeed int,
	caves map[string]*Cave, cavesMutex *sync.RWMutex, c chan *Chunk) {
	//fmt.Printf("Making a chunk for %d,%d", chunkX, chunkZ)
	fileName := filepath.Join(worldPath, fmt.Sprintf("%d-%d.dat", chunkX, chunkZ))
	chunkKey := fmt.Sprintf("%d,%d", chunkX, chunkZ)
	var chunk = &Chunk{}
	if _, err := os.Stat(fileName); err == nil {
		chunk = load(fileName)
	} else {
		// bunch of rng's for different cube column properties
		cubeLayersChunkSeed := perlin.ChunkHash(uint(cubeLayersSeed), chunkX, chunkZ)
		cubeLayersChunkRand := rand.New(rand.NewSource(int64(cubeLayersChunkSeed)))
		blendChunkSeed := perlin.ChunkHash(uint(blendSeed), chunkX, chunkZ)
		blendChunkRand := rand.New(rand.NewSource(int64(blendChunkSeed)))
		cubeTypeChunkSeed := perlin.ChunkHash(uint(cubeTypeSeed), chunkX, chunkZ)
		cubeTypeChunkRand := rand.New(rand.NewSource(int64(cubeTypeChunkSeed)))
		cubeHeightChunkSeed := perlin.ChunkHash(uint(cubeHeightSeed), chunkX, chunkZ)
		cubeHeightChunkRand := rand.New(rand.NewSource(int64(cubeHeightChunkSeed)))

		chunk.Filled = make([]byte, numCubes)
		chunk.CubeIndices = make([]byte, numCubes)
		for x := 0; x < chunkSize; x++ {
			for z := 0; z < chunkSize; z++ {
				cubeLayersRand := cubeLayersChunkRand.Float32()
				blendRand := blendChunkRand.Float32()
				cubeTypeRand := cubeTypeChunkRand.Float32()
				cubeHeightRand := cubeHeightChunkRand.Float32()
				index := x*chunkSize*chunkHeight + z*chunkHeight
				biomes.FillCubeColumn(
					x+chunkX*chunkSize,
					z+chunkZ*chunkSize,
					chunkX, chunkZ, index,
					cubeLayersRand, blendRand, cubeTypeRand, cubeHeightRand,
					chunk.Filled, chunk.CubeIndices)
				/*height := perlinGrid.GetPixel(x+chunkX*chunkSize, z+chunkZ*chunkSize, 4, 128)
				for y := 0; y < chunkHeight; y++ {
					var index = x*chunkSize*chunkHeight + z*chunkHeight + y
					chunk.CubeIndices[index] = DIRT_CUBE_INDEX
					if byte(y) <= WATER_LEVEL && height <= byte(y) {
						chunk.CubeIndices[index] = WATER_CUBE_INDEX
						chunk.Filled[index] = 1
					}
					if height > byte(y) {
						chunk.Filled[index] = 1
					}
				}*/
			}
		}
		chunk.key = chunkKey
		cavesMutex.RLock()
		for _, cave := range caves {
			cave.applyToChunk(chunk)
		}
		cavesMutex.RUnlock()
	}
	chunk.vertices = make([]float32, 0)
	chunk.indices = make([]uint32, 0)
	chunk.X = chunkX
	chunk.Y = chunkY
	chunk.Z = chunkZ
	chunk.bounds = getBounds(chunkX, chunkY, chunkZ)
	chunk.updateChunk()
	// they are slices, can't just print them
	//fmt.Printf("%v", chunk.indices)
	//fmt.Printf("%v", chunk.vertices)
	//log.Fatal("wat")
	//fmt.Printf("Size of vertices: %d", len(chunk.vertices))
	// so, looking a lot better in terms of moving seamlessly but still seeing some slowdowns
	// so going to offload saving to a new goroutine since it should be fine to let this happen
	// in the background (err, maybe not. what if you unload then reload and the save hasn't completed...)
	chunk.save(fileName)
	chunk.changed = false
	chunk.dirty = false
	chunk.key = chunkKey
	//fmt.Printf("Made the chunk for %s", chunk.key)
	c <- chunk
}

// ok, so new theory - the massively growing memory chunks
// are coming from opengl. the slow growing chunks when at rest, im not so sure about,
// but this make sense w.r.t what im seeing when i start driving through the world
// plan is to just cleanup the stuff i genned
func (chunk *Chunk) deleteFromOpengl() {
	if chunk.vao != 0 {
		gl.DeleteVertexArrays(1, &chunk.vao)
	}
	if chunk.vbo != 0 {
		gl.DeleteBuffers(1, &chunk.vbo)
	}
	if chunk.ebo != 0 {
		gl.DeleteBuffers(1, &chunk.ebo)
	}
}

func (chunk *Chunk) loadIntoOpengl() {
	// ok, so going cube by cube was way too inefficient (i think). lets try rendering at the chunk level instead
	// the vertex array object encapsulates vertex buffer objects along with glEnable/DisableVertexAttribArray
	// and calls to glVertexAttribPointer
	// more abstract/confusing than the ebo or vbo
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	// the vertex buffer object makes more sense, it's basically just holding a bunch of points
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	// bind to the array buffer context in the opengl state machine
	// this means subsequent array buffer calls we make are going to modify
	// vbo (remember its a state machine)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	// copy our points [] into the vbo object in opengl,
	// static draw tells it that we don't plan on changing those points at all
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(chunk.vertices), gl.Ptr(chunk.vertices), gl.STATIC_DRAW)

	// ebos act as a lookup map for vertices(give me an index, ill give u a vertex). this saves memory when ur rendering
	var ebo uint32
	gl.GenBuffers(1, &ebo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(chunk.indices)*4, gl.Ptr(chunk.indices), gl.STATIC_DRAW)

	// this deserves a good explanation
	// first param is the vertex attribute we want to have rendered
	// basically, given the big buffer of data we setup, we need
	// to tell opengl where to start in there. it looks like this actually lines
	// up with the position we pick for "location" in our vertext shader.
	// second param is how big each vertex/point is. we have 3 values
	// per each (x,y,z), so its 3.
	// third param is the type of the values (we are using floats)
	// fourth param is whether the values should be transformed in
	// some way to be normalized, apparently people apply this when
	// storing their vertex/point in bytes or as ints,
	// not sure why/when you'd want to use this
	// fifth parameter tells you how far to jump to go from one vertex/pt
	// to the next.now we're storing textures, so the vertices are not tightly packed.
	// sixth parameter is the offset to begin at in the array, not quite
	// sure how this is applied to the first param, maybe its added to
	// the 1st param times the second param to get the starting spot?
	// why does using an ebo make us need a stride???
	// it doesn't - don't quite understand what this does here, maybe
	// opengl can logically figure out your stride if things are tightly
	// packed, so passing 0 tells it hey, stride by 3* sizeof(float)?
	// this call is stored inside the vao "state" we created above
	// structure is [R G B X Y], each is a float, so 4 bytes
	// treat it as [[R G B],[X Y]]
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 5*4, nil)
	// this call is stored inside the vao "state" we created above
	gl.EnableVertexAttribArray(0)
	// AHHHHHHHHHHHHH, you need another call to tell opengl about your texture coords
	// index 1 = [X,Y], it is 2 elements large, and there are 5 elements (of size 4 each) total in a structure
	// offset is the first 3 elements * size of 4 each
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 5*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)
	// ok, apparently vbos just draw one single object (which is a collection)
	// of points/vertices. opengl wants to make that process more efficient
	// so it has a data structure ( a vao ) that will store the stuff we did
	// here, specifically enabling the vertex attribute, configuring how
	// to load in our vertices (and from where) with vertexattribpointer
	// according to the book, vaos are composed of attribute pointers
	// and the 1st pointer points to 1st vertex/ knows how to get the 2nd vertex, etc
	// but u can have more pointers in the vao that point to other lists
	// stored in the same vbo but skipped over by the stride. we're tightly
	// packed so our stride is 0, so im guessing its just 1 ptr for us, but
	// wondering why you would use the other vao attribute pointers
	chunk.vao = vao
	chunk.vbo = vbo
	chunk.ebo = ebo
}

// well, i crashes my computer on this. current theory
// is that this is actually horribly inefficent, as im probably
// copying the whole array into z/y/c?

func (chunk *Chunk) draw() {
	// apparently, when updating an ebo this call has to come first, no specific reason or documentation provided around it...
	gl.BindVertexArray(chunk.vao)
	// ok, so ideally i would use gl.subbufferdata here instead of rebuilding the entire chunk and rerendering it.
	// otoh, these updates appear to happen in sub 20/10ms time, so i don't think its worth the optimization
	if chunk.dirty {
		var drawUpdatedChunkTime = time.Now()
		gl.BindBuffer(gl.ARRAY_BUFFER, chunk.vbo)
		// copy our points [] into the vbo object in opengl,
		// static draw tells it that we don't plan on changing those points at all
		gl.BufferData(gl.ARRAY_BUFFER, 4*len(chunk.vertices), gl.Ptr(chunk.vertices), gl.STATIC_DRAW)
		gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, chunk.ebo)
		gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(chunk.indices)*4, gl.Ptr(chunk.indices), gl.STATIC_DRAW)
		chunk.dirty = false
		fmt.Printf("chunk.draw() elapsedTime: %d\n", time.Since(drawUpdatedChunkTime)/1000000)
	}
	// gonna hardcode it again lmao (number of indices)
	// literally wrote it wrong lmao
	gl.DrawElements(gl.TRIANGLES, int32(len(chunk.indices)), gl.UNSIGNED_INT, nil)
	printGlError()
}
