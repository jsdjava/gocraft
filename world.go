package main

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gocraft/biomes"
	"gocraft/perlin"

	"github.com/go-gl/mathgl/mgl32"
)

// plan is to render 16 chunks in the x and z directions
// i guess y is already tall enough - confirmed
const numChunks int = 7 //15 //im dumb, it IS 15 in both directions aggg

type World struct {
	cubeLayersSeed int
	blendSeed      int
	cubeTypeSeed   int
	cubeHeightSeed int
	chunks         map[string]*Chunk
	caves          map[string]*Cave
	x              int
	z              int
	bgLoad         int
	loadX          int
	loadZ          int
	bgChan         chan bool
	chunksMutex    sync.RWMutex
	cavesMutex     sync.RWMutex
	caveSeed       uint
	biomes         *biomes.Biomes
	//perlinGrid  *perlin.PerlinGrid
}

const (
	IDLE      = 0
	LOADING   = 1
	COMPLETED = 2
)

const (
	POSITIVE int = 1
	NEGATIVE int = -1
)

const (
	X string = "x"
	Z string = "z"
)

const CAVE_LENGTH = 100

// this 4 is hardcoded as ppv in cave
const MAX_CAVE_CHUNKS = (CAVE_LENGTH*4)/chunkSize + 1

// TODO Temporary for testing
// var caves = []*Cave{makeCave(0, 130, 0, 100)}

func (world *World) loadNewCaves(sign int, axis string, overrideX int) {
	for i := -numChunks; i <= numChunks; i++ {
		chunkX := overrideX + numChunks + 1
		chunkZ := i + world.z
		if axis == X && sign == NEGATIVE {
			chunkX = overrideX - numChunks - 1
			chunkZ = i + world.z
		} else if axis == Z && sign == POSITIVE {
			chunkX = i + overrideX
			chunkZ = world.z + numChunks + 1
		} else if axis == Z && sign == NEGATIVE {
			chunkX = i + overrideX
			chunkZ = world.z - numChunks - 1
		}
		world.makeCave(chunkX, chunkZ)
	}
}

func (world *World) makeCave(chunkX int, chunkZ int) {
	chunkSeed := perlin.ChunkHash(world.caveSeed, chunkX, chunkZ)
	caveRand := rand.New(rand.NewSource(int64(chunkSeed)))
	if caveRand.Intn(100) >= 98 {
		blendChunkSeed := perlin.ChunkHash(uint(world.blendSeed), chunkX, chunkZ)
		blendChunkRand := rand.New(rand.NewSource(int64(blendChunkSeed)))
		chunkKey := fmt.Sprintf("%d,%d", chunkX, chunkZ)
		// TODO I think this is random???
		seeds := []uint{
			uint(caveRand.Uint32()),
			uint(caveRand.Uint32()),
			uint(caveRand.Uint32()),
			uint(caveRand.Uint32()),
		}
		// TODO Duped between chunk.go and here..
		// TODO Means the cave gets genned right at the start of a chunk
		xStart := chunkX * chunkSize
		zStart := chunkZ * chunkSize
		yStart, _ := world.biomes.GetHeightAndBiome(chunkX*chunkSize, chunkZ*chunkSize, chunkX, chunkZ, blendChunkRand.Float32()) //world.perlinGrid.GetPixel(chunkX*chunkSize, chunkZ*chunkSize, 4, 128)
		c := makeCave(xStart, int(yStart), zStart, seeds, CAVE_LENGTH)
		world.cavesMutex.Lock()
		world.caves[chunkKey] = c
		world.cavesMutex.Unlock()
	}
}

// as we move through a world, we need to add more chunks in the direction we are moving
// we also need to store chunks on disk as we move past them, and then either load them from
// disk or generate them
// NUM CHUNKS NOT CHUNK SIZE AHHHHHHHHHHHHHH
// why do i need overrideX, you might ask. well, world.x isn't threadsafe, but to fake
// out diagonal moves im just composing them of vertical + horizontal. i can just pick one
// axis to do the faked out move on (x)
func (world *World) loadNewChunks(sign int, axis string, overrideX int, c chan *Chunk) {
	//fmt.Printf("Loading chunks for %d,%s", sign, axis)
	for i := -numChunks; i <= numChunks; i++ {
		// i think you can multiply by the sign in here (numChunks+1), and numChunksSize and drop the number of branches to just 2,
		// but will do that after coding the 1st version of it
		// axis == X, sign = POSITIVE
		chunkX := overrideX + numChunks + 1
		chunkZ := i + world.z
		// duh, forgot to uncomment these
		//unloadKey := fmt.Sprintf("%d,%d", world.x-numChunks, i+world.z)
		if axis == X && sign == NEGATIVE {
			chunkX = overrideX - numChunks - 1
			// ya, this is redundant
			chunkZ = i + world.z
			//unloadKey = fmt.Sprintf("%d,%d", world.x+numChunks, i+world.z)
		} else if axis == Z && sign == POSITIVE {
			chunkX = i + overrideX
			chunkZ = world.z + numChunks + 1
			//unloadKey = fmt.Sprintf("%d,%d", i+world.x, world.z-numChunks)
		} else if axis == Z && sign == NEGATIVE {
			chunkX = i + overrideX
			chunkZ = world.z - numChunks - 1
			//unloadKey = fmt.Sprintf("%d,%d", i+world.x, world.z+numChunks)
		}
		// did not realize i was using a global here, probably want to rethink this
		go makeChunk(
			world.biomes,
			chunkX, 0, chunkZ, cubes,
			world.cubeLayersSeed, world.blendSeed, world.cubeTypeSeed, world.cubeHeightSeed,
			world.caves, &world.cavesMutex, c,
		)
		//unloadKeys = append(unloadKeys, unloadKey)
		//fmt.Printf("Loaded chunk at %d,%d\n", chunkX, chunkZ)
	}
}

// ok, so just driving in one cardinal direction seems ok - i have a theory why the diagonals
// are worse. i believe its because loadNewChunks waits for all 15 goroutines to complete, then we
// have to wait for the next 15, instead of batching all 30 at once
func (world *World) processMove(direction string) {
	start := time.Now()
	c := make(chan *Chunk)
	loadedChunks := numChunks*2 + 1
	//fmt.Printf(direction)
	switch direction {
	case "0,1":
		world.loadNewCaves(POSITIVE, Z, world.x)
		world.loadNewChunks(POSITIVE, Z, world.x, c)
	case "1,1":
		world.loadNewCaves(POSITIVE, X, world.x)
		world.loadNewCaves(POSITIVE, Z, world.loadX)
		world.loadNewChunks(POSITIVE, X, world.x, c)
		world.loadNewChunks(POSITIVE, Z, world.loadX, c)
		loadedChunks *= 2
	case "1,0":
		world.loadNewCaves(POSITIVE, X, world.x)
		world.loadNewChunks(POSITIVE, X, world.x, c)
	case "1,-1":
		world.loadNewCaves(POSITIVE, X, world.x)
		world.loadNewCaves(NEGATIVE, Z, world.loadX)
		world.loadNewChunks(POSITIVE, X, world.x, c)
		world.loadNewChunks(NEGATIVE, Z, world.loadX, c)
		loadedChunks *= 2
	case "0,-1":
		world.loadNewCaves(NEGATIVE, Z, world.x)
		world.loadNewChunks(NEGATIVE, Z, world.x, c)
	case "-1,-1":
		world.loadNewCaves(NEGATIVE, X, world.x)
		world.loadNewCaves(NEGATIVE, Z, world.loadX)
		world.loadNewChunks(NEGATIVE, X, world.x, c)
		world.loadNewChunks(NEGATIVE, Z, world.loadX, c)
		loadedChunks *= 2
	case "-1,0":
		world.loadNewCaves(NEGATIVE, X, world.x)
		world.loadNewChunks(NEGATIVE, X, world.x, c)
	case "-1,1":
		world.loadNewCaves(NEGATIVE, X, world.x)
		world.loadNewCaves(POSITIVE, Z, world.loadX)
		world.loadNewChunks(NEGATIVE, X, world.x, c)
		world.loadNewChunks(POSITIVE, Z, world.loadX, c)
		loadedChunks *= 2
	}
	// need to be very careful to maintain this order:
	// - add new chunks to map
	// - update world position
	// - delete old chunks from map
	// actually, we could potentially change this order because everything happens in the render loop...
	// welp, now im going to make this its own go routine, so, we do need it to be threadsafe
	// now, b/c its a ptr to world, the set should actually go through
	chunks := make([]*Chunk, loadedChunks)
	//fmt.Printf("waiting for my loaded chunks")
	for i := 0; i < loadedChunks; i++ {
		//fmt.Print(i)
		// this isn't the right way to do this - i should read all the chunks from the channel,
		// then push them onto world chunks as one group instead of locking/unlocking between each one
		// but im trying to understand how this is causing a chunk to be nil?
		// AHHHHHHHHHHH WE ARE LOADING CHUNKS IN AND THEN DELETING THEM
		// really, we need the world position update to go through here AS WELL before we unlock the mutex
		// i knew there was a reason free deletion was stupid aggggggggggggggggggggggggggggggggggg
		// yeah, so by appending i was actually adding at the 8th, 9th, etc spots
		chunks[i] = <-c
	}
	//fmt.Printf("locking chunks mutex")
	world.chunksMutex.Lock()
	for i := 0; i < len(chunks); i++ {
		chunk := chunks[i]
		world.chunks[chunk.key] = chunk
	}
	world.x = world.loadX
	world.z = world.loadZ
	world.chunksMutex.Unlock()
	//fmt.Printf("unlocking chunks mutex")
	//fmt.Printf("Total chunks loaded: %d\n", len(world.chunks))
	//world.chunksMutex.Lock()
	//for i := 0; i < len(unloadKeys); i++ {
	//	world.chunks[unloadKeys[i]].deleteFromOpengl()
	//	delete(world.chunks, unloadKeys[i])
	//}
	//world.chunksMutex.Unlock()
	world.bgLoad = COMPLETED
	// so, these hang the goroutine until they are read
	world.bgChan <- true

	elapsed := time.Since(start)
	fmt.Println(elapsed)
}

// apparently pointers just magically dereference, works for me
// note, this is NOT threadsafe, need to be careful
func (world *World) onMove(x float32, z float32, textures []Texture) {
	var bgLoad = world.bgLoad
	// always drain the channel when the bgthread has finished
	if bgLoad == COMPLETED {
		<-world.bgChan
		world.bgLoad = IDLE
		bgLoad = IDLE
		fmt.Printf("completed load of a new section\n")
	}
	newX := int(x / (float32(chunkSize) * cs))
	newZ := int(z / (float32(chunkSize) * cs))
	deltaX := newX - world.x
	deltaZ := newZ - world.z
	direction := fmt.Sprintf("%d,%d", deltaX, deltaZ)
	if direction != "0,0" {
		if bgLoad == IDLE {
			fmt.Printf("beginning load of a new section\n")
			world.loadX = newX
			world.loadZ = newZ
			world.bgLoad = LOADING
			go world.processMove(direction)
		} else if !(world.loadX == newX && world.loadZ == newZ) {
			fmt.Printf("we are behind, loading synchronously\n")
			// ok, this is bad, it means we are behind (so behind)
			// that the bg thread hasn't finished loading but we've
			// moved into yet another new chunk space.
			// wait for the first load to complete
			<-world.bgChan
			//fmt.Printf("ok, lets do the next call")
			world.loadX = newX
			world.loadZ = newZ
			// recompute the actual direction because otw we are using the old one, causing a direction of -2,-2 for example
			deltaX := newX - world.x
			deltaZ := newZ - world.z
			direction := fmt.Sprintf("%d,%d", deltaX, deltaZ)
			// arguably, could just not spawn the goroutine here...
			go world.processMove(direction)
			// ALSO, wait for the second load to complete, hopefully we won't be so far
			// behind again
			//fmt.Printf("waiting for 2nd resp")
			<-world.bgChan
			fmt.Printf("caught up (for now)\n")
			world.bgLoad = IDLE
		}
	}
}

func chunkIndex(v int) int {
	return int(math.Floor(float64(v) / (float64(chunkSize) * float64(cs))))
}

func cubeIndex(v float32) int {
	// i think the ceil/floor allow these to go 1 higher at the bounds. we still want to ceil/floor, but should probably
	// enforce the range of 0 to chunkSize-1
	ci := 0
	if v >= 0 {
		ci = int(math.Ceil(math.Mod(float64(v), float64(float32(chunkSize)*cs))) / float64(cs))
	} else {
		ci = chunkSize - 1 + int(math.Floor(math.Mod(float64(v), float64(float32(chunkSize)*cs)))/float64(cs))
	}
	// maybe not, i had that cubeindex(y) bug in there
	if ci > chunkSize-1 {
		return chunkSize - 1
	} else if ci < 0 {
		return 0
	}
	return ci
}

func cubeMinIndex(v float32) int {
	return 0
}

func cubeMaxIndex(v float32) int {
	return chunkSize - 1
}

type Point struct {
	x int
	z int
}

func (world *World) buildCube(
	pos mgl32.Vec3,
	playerX float32, playerY float32, playerZ float32,
	playerWidth float32, playerHeight float32, playerDepth float32,
) bool {
	xIndex, worldY, zIndex := world.getCubeCoordinates(pos.X(), pos.Y(), pos.Z())
	chunkKey := world.getChunkKey(pos.X(), pos.Z())
	if chunk, ok := world.chunks[chunkKey]; ok {
		chunk.tryBuildCube(xIndex, zIndex, worldY)
		if !(world.intersecting(playerX, playerY, playerZ, playerWidth, playerHeight, playerDepth)) {
			chunk.updateChunk()
			return true
		}
		chunk.undoBuildCube(xIndex, zIndex, worldY)
	}
	return false
}

func (world *World) destroyCube(pos mgl32.Vec3) {
	xIndex, worldY, zIndex := world.getCubeCoordinates(pos.X(), pos.Y(), pos.Z())
	chunkKey := world.getChunkKey(pos.X(), pos.Z())
	if chunk, ok := world.chunks[chunkKey]; ok {
		chunk.destroyCube(xIndex, zIndex, worldY)
	}
}

func (world *World) getChunkKey(x float32, z float32) string {
	worldX := int(math.Floor(float64(x)))
	worldZ := int(math.Floor(float64(z)))
	chunkX := chunkIndex(worldX)
	chunkZ := chunkIndex(worldZ)
	chunkKey := fmt.Sprintf("%d,%d", chunkX, chunkZ)
	return chunkKey
}

func (world *World) getCubeCoordinates(x float32, y float32, z float32) (int, int, int) {
	// this is why you need to sleep, this bug probably cost me 3 hours
	worldX := int(math.Floor(float64(x)))
	worldY := int(y / cs)
	worldZ := int(math.Floor(float64(z)))
	chunkX := chunkIndex(worldX)
	chunkZ := chunkIndex(worldZ)
	xIndex := worldX % chunkSize
	zIndex := worldZ % chunkSize
	if chunkX < 0 {
		xIndex = chunkSize - 1 + (worldX+1)%chunkSize
	}
	if chunkZ < 0 {
		zIndex = chunkSize - 1 + (worldZ+1)%chunkSize
	}
	return xIndex, worldY, zIndex
}

// TODO Also need to factor in cs for x/z
func (world *World) filled(x float32, y float32, z float32) bool {
	xIndex, worldY, zIndex := world.getCubeCoordinates(x, y, z)
	chunkKey := world.getChunkKey(x, z)
	world.chunksMutex.RLock()
	chunk, ok := world.chunks[chunkKey]
	world.chunksMutex.RUnlock()
	if ok {
		return chunk.filled(xIndex, zIndex, worldY)
	}
	return false
}

func (world *World) intersecting(x float32, y float32, z float32, width float32, height float32, depth float32) bool {
	// feels like i seriously overcomplicated this.
	// basically, i know all the cubes i need to check. they are the cubes from
	// x to x+width, z to z+depth, y to y+height
	// i just need to segment those cubes into their different chunk(s), and call for them
	// TODO Factor in cs for x and z...
	yMin := int(y / cs)
	yMax := int(math.Ceil(float64((y + height) / cs)))
	for i := int(math.Floor(float64(x))); i < int(math.Ceil(float64(x+width))); i++ {
		for j := int(math.Floor(float64(z))); j < int(math.Ceil(float64(z+depth))); j++ {
			chunkX := chunkIndex(i)
			chunkZ := chunkIndex(j)
			chunkKey := fmt.Sprintf("%d,%d", chunkX, chunkZ)
			world.chunksMutex.RLock()
			chunk, ok := world.chunks[chunkKey]
			world.chunksMutex.RUnlock()
			if ok {
				xIndex := i % chunkSize
				zIndex := j % chunkSize
				// reeeeeeeeeeeeeeeeeeeeeeeeeeee
				// when u are at -0.5, for example, you don't want to begin at -1, you want
				// to begin at 0, hence the stupid i+1/j+1
				// we also have to invert the index 15=0, 14=1, etc because of how (-) chunk dimensions
				// get rendered
				if chunkX < 0 {
					xIndex = chunkSize - 1 + (i+1)%chunkSize
				}
				if chunkZ < 0 {
					zIndex = chunkSize - 1 + (j+1)%chunkSize
				}
				//fmt.Printf("%f,%f - %s - %d,%d\n", x, z, chunkKey, xIndex, zIndex)
				if chunk.intersecting(xIndex, zIndex, yMin, yMax) {
					return true
				}
			} else {
				// can't guarantee a chunk we are in is loaded (think startup), so lets just return true
				// until it has loaded
				return true
			}
		}
	}
	return false

	/*x2 := x + width
	z2 := z + depth
	// figure out what chunk(s) we are in
	// this same mistake may be why perlin noise is off at the axes
	minX := min(chunkIndex(x), chunkIndex(x2))
	maxX := max(chunkIndex(x), chunkIndex(x2))
	minZ := min(chunkIndex(z), chunkIndex(z2))
	maxZ := max(chunkIndex(z), chunkIndex(z2))
	//fmt.Printf("%d,%d,%d,%d %d,%d,%d,%d,%d,%d\n", minX, minZ, maxX, maxZ, cubeIndex(x), cubeIndex(x2), cubeMaxIndex(x), cubeMinIndex(x), cubeMaxIndex(x2), cubeMinIndex(x2))
	// 4 chunks case\n
	if minX != maxX && minZ != maxZ {
		//fmt.Printf("4 chunks\n")
		return world.chunks[fmt.Sprintf("%d,%d", minX, maxZ)].intersecting(cubeIndex(x), cubeMaxIndex(x), cubeMinIndex(z2), cubeIndex(z2), yMin, yMax, x, z) ||
			world.chunks[fmt.Sprintf("%d,%d", maxX, maxZ)].intersecting(cubeMinIndex(x2), cubeIndex(x2), cubeMinIndex(z2), cubeIndex(z2), yMin, yMax, x, z) ||
			world.chunks[fmt.Sprintf("%d,%d", maxX, minZ)].intersecting(cubeMinIndex(x2), cubeIndex(x2), cubeIndex(z), cubeMaxIndex(z), yMin, yMax, x, z) ||
			world.chunks[fmt.Sprintf("%d,%d", minX, minZ)].intersecting(cubeIndex(x), cubeMaxIndex(x), cubeIndex(z), cubeMaxIndex(z), yMin, yMax, x, z)
	} else if minX != maxX {
		//fmt.Printf("2 chunks, in X\n")
		// 2 chunks, x crossing
		return world.chunks[fmt.Sprintf("%d,%d", minX, minZ)].intersecting(cubeIndex(x), cubeMaxIndex(x), cubeIndex(z), cubeIndex(z2), yMin, yMax, x, z) ||
			world.chunks[fmt.Sprintf("%d,%d", maxX, minZ)].intersecting(cubeMinIndex(x2), cubeIndex(x2), cubeIndex(z), cubeIndex(z2), yMin, yMax, x, z)

	} else if minZ != maxZ {
		//fmt.Printf("2 chunks, in z\n")
		// 2 chunks, z crossing
		return world.chunks[fmt.Sprintf("%d,%d", minX, minZ)].intersecting(cubeIndex(x), cubeIndex(x2), cubeIndex(z), cubeMaxIndex(z), yMin, yMax, x, z) ||
			world.chunks[fmt.Sprintf("%d,%d", minX, maxZ)].intersecting(cubeIndex(x), cubeIndex(x2), cubeMinIndex(z2), cubeIndex(z2), yMin, yMax, x, z)
	} else {
		//fmt.Printf("just one chunk\n")
		// 1 chunk
		return world.chunks[fmt.Sprintf("%d,%d", minX, minZ)].intersecting(cubeIndex(x), cubeIndex(x2), cubeIndex(z), cubeIndex(z2), yMin, yMax, x, z)
	}
	// case 1
	// ok, when x is negative, position is reversed in the chunk. ditto for z
	/*if x >= 0 {
		relX := math.Mod(float64(x), float64((float32(chunkSize) * cs)))
	} else {
		relX = chunkSize - 1 + int(x)%(chunkSize*int(cs))
	}
	if z >= 0 {
		relZ = int(z) % (chunkSize * int(cs))
	} else {
		relZ = chunkSize - 1 + int(z)%(chunkSize*int(cs))
	}*/
	// return world.chunks[fmt.Sprintf("%d,%d", chunkX, chunkZ)].intersecting(relX, int(y/cs), relZ, width, height, depth)
}

var worldPath = filepath.Join(".", "world")

// may introduce threads eventually, will not do that yet
// 9s to load a world (no threads)
// 1m36s to load a world???
// 16.59s
func loadWorld(cubes []*Cube) *World {
	start := time.Now()
	os.MkdirAll(worldPath, os.ModePerm)
	c := make(chan *Chunk)
	var world = World{}
	world.bgChan = make(chan bool)
	world.chunks = make(map[string]*Chunk)
	world.caves = make(map[string]*Cave)
	world.biomes = biomes.MakeBiomes()
	//world.perlinGrid = perlin.MakeGrid(4, 9, 26)
	world.cubeLayersSeed = 2
	world.blendSeed = 99999
	world.cubeTypeSeed = 9090
	world.cubeHeightSeed = 7878
	world.caveSeed = 99
	for x := -numChunks - MAX_CAVE_CHUNKS; x <= numChunks+MAX_CAVE_CHUNKS; x++ {
		for z := -numChunks - MAX_CAVE_CHUNKS; z <= numChunks+MAX_CAVE_CHUNKS; z++ {
			world.makeCave(x, z)
		}
	}
	for x := -numChunks; x <= numChunks; x++ {
		for z := -numChunks; z <= numChunks; z++ {
			go makeChunk(
				world.biomes,
				x, 0, z,
				cubes,
				world.cubeLayersSeed, world.blendSeed, world.cubeTypeSeed, world.cubeHeightSeed,
				world.caves, &world.cavesMutex, c,
			)
		}
	}
	for i := 0; i < (numChunks*2+1)*(numChunks*2+1); i++ {
		chunk := <-c
		world.chunks[chunk.key] = chunk
	}
	elapsed := time.Since(start)
	fmt.Printf("Took %s to load world", elapsed)
	return &world
}

func (world *World) draw(planes []Plane) {
	// my guess is that what's happening is we are inside these loops
	// when the delete? to the map is happening?
	world.chunksMutex.Lock()
	// feels kind of dumb, can probably do this better
	// oh look, it is dumb
	keysToDelete := make(map[string]bool, len(world.chunks))
	for k := range world.chunks {
		keysToDelete[k] = true
	}
	for x := -numChunks; x <= numChunks; x++ {
		for z := -numChunks; z <= numChunks; z++ {
			key := fmt.Sprintf("%d,%d", x+world.x, z+world.z)
			keysToDelete[key] = false
			// go has some weird rules about things you can do with map values
			// https://webapplicationconsultant.com/go-lang/cannot-assign-to-struct-field-in-map/
			// https://utcc.utoronto.ca/~cks/space/blog/programming/GoAddressableValues
			// this SHOULD be copying a ptr, not a value
			chunk := world.chunks[key]
			if chunk.vao == 0 {
				chunk.loadIntoOpengl()
				world.chunks[key] = chunk
			}
			if chunk.inFrustrum(planes) {
				chunk.draw()
			} else {
				//fmt.Printf("Skipping %s", key)
			}
		}
	}
	for k := range keysToDelete {
		if keysToDelete[k] {
			chunk := world.chunks[k]
			chunk.deleteFromOpengl()
			delete(world.chunks, k)
		}
	}
	world.chunksMutex.Unlock()
	world.cavesMutex.Lock()
	keysToDelete = make(map[string]bool, len(world.caves))
	for k := range world.caves {
		keysToDelete[k] = true
	}
	for x := -numChunks - MAX_CAVE_CHUNKS; x <= numChunks+MAX_CAVE_CHUNKS; x++ {
		for z := -numChunks - MAX_CAVE_CHUNKS; z <= numChunks+MAX_CAVE_CHUNKS; z++ {
			key := fmt.Sprintf("%d,%d", x+world.x, z+world.z)
			keysToDelete[key] = false
		}
	}
	for k := range keysToDelete {
		if keysToDelete[k] {
			delete(world.caves, k)
		}
	}
	world.cavesMutex.Unlock()
}
