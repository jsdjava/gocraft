package main

import (
	"fmt"
	"log"
	"math"
	"runtime"
	"strings"
	"time"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

const (
	width  = 750
	height = 750
)

var world *World
var player *Player
var sun *Sun
var textures []Texture
var cubes []*Cube

// ok good, starting to understand the projection matrix a little bit.
// basically, i must be at least -0.1 but > -10 on the z-axis to see this thing,
// which is what i set my far and near planes to be

// still don't quite understand where the square we're defining (-0.5 to 0.5 in x and y dirs) stops filling
// the whole screen and starts leaving more empty space, but can figure that out
// later
// put the z back to zero, im now shifting the whole scene backwards a little with a view matrix

// ok, lets try rotating 45 degrees on the z axis
// how are go types so simple ???
// its as if mgl32.Vec3 just aliases to []float32
// haha ur rotating on the x axis, not the z...............................
// z isn't right though, that is just going to turn the square into a rotated square,
// we want it to have parts that are further away from us (that vanish). rotating on X or Y axis
// shoulddddddd cause this
// successs baby.
// this makes more sense now, the reason 45 degrees is rotating it fully off the screen is because i had to move the
// object off the z axis a little (back) so it wouldn't get clipped away. when i rotate, its not just rotating as if its centered
// around the z axis, its slightly away from it, so its sort of on a circle around me (in the z-axis), and im rotating it so its not
// quite in my FoV. tricky, and a good reason for me to put the z's back to 0 and get that view matrix in so i stop confusing myself
// ayyyy, now that i moved the camera back and rotated the object around its center, things are looking good

// haha i left a weird rotation on this and then got confused when doing backface culling.
// please, just place objects directly inside the world with no rotations or translations (except maybe moving them backwards)
// in the z a little bit
var modelMatrix = mgl32.HomogRotate3D(mgl32.DegToRad(0), mgl32.Vec3{0, 0, 0})

// perspective causes parallel lines to "vanish" as they get further away, and it should make our
// "rectangle" (orthographic view) of our square turn into a perspective view of a square titled at 45 degrees,
// i.e. in the back, the height of it will be smaller
// book says you normally do 45 degrees for FoV, and then we need to tell it our AR (so in our case the ratio of width to height of our window)
// and we need to tell it where the near plane and the far plane are (remember, this is a small plane, expanding to a larger plane, all the space held between them)
// careful, this is in 2 places
var distanceFar float32 = 1000.0
var distanceNear float32 = 0.1
var ratio = float32(width) / float32(height)
var projectionMatrix = mgl32.Perspective(mgl32.DegToRad(fov), ratio, distanceNear, distanceFar)

// Lets use a view matrix so we can place our object at the center of the world and just back ourselves up a little bit
// before rotating around it (at the center)
// I basically just want to translate us along the + z axis a little, think that LookAt is this libs
// equivalent of that
// Ok, this is more complicated, theres a whole new chapter on it. I'm just going to build the translation matrix myself
// start with identity matrix, to sanity check
// move back along the z axis
// its negative because we basically want to shift the whole scene 3 away from us, and the z-axis points positively at us,
// so to shift things away we use negative

// ok, now its lookat time. basically, we position a "camera" in our
// world, and then we transform the world by that camera's view (so basically making the world appear to us as if we are the camera),
// where our projection matrix can then finish by painting it onto a 2d scene
// first 3 are the camera's position (along the + z axis, i think)
// next 3 are the world's center (ya its just 0,0,0)
// last 3 are pointing upwards (duh, just 0,1,0)
// its camera time, we're going to move this camera
// ITS IMPORTANT X AND Z STAY 0, ITS HOW NEW CHUNKS GET LOADED CORRECTLY
// if u want to change this, peek into the world.onMove code
//var cameraPos = mgl32.Vec3{10, 128, 0} // y=128

// ahh, i fundamentally misunderstood this. this vector is a normalized vector that determines where the camera is pointing.
// this is important, because it means that it always has to have a length of 1 (total). in order to figure out where we are pointing,
// we can take our cameraPos, subtract the origin from it, and normalize that. that results in a vector of 0,0,-1. not totally confident
// on the sign here yet
var cameraFront = mgl32.Vec3{0, 0, -1}
var cameraUp = mgl32.Vec3{0, 1, 0}

// cameraFront is what we can use to rotate the camera. right now, we're basically saying point the camera at the origin of the world, but
// we can point it in any other direction as well. cameraPos is how we move the camera throughout the world. by pointing it and moving it,
// we can do pretty much everything we want (except zoom in and out).
// i think the reason cameraFront is added to cameraPos is because otherwise you would zoom in/out as you moved the camera (b/c our direction vector is normalized)
// not totally sure why we don't just pass the direction vector and have lookatv handle it though...
// feels like it shouldn't matter, im just defining where the camera is pointing...
var viewMatrix = mgl32.Ident4() //.LookAtV(cameraPos, cameraFront.Add(cameraPos), cameraUp)

// this looks weird when rotated on the x axis - i thinkkkkkkk this is
// because its orthogonal, so while perspective would make this look like it was slowly
// diminishing to a point, w/o perspective you basically just have a square face being "stretched"
// along the direction you are rotating. z-axis looks ok because we're directly on top of it (remember z=0 for all pts right now)

// remember, this is defining what happens right at the start
// of the opengl pipeline, and its being applied to each point
// we pass into it, basically, we tell it that its getting a
// point (the layout ... line) and then we define the transform
// to the point (pretty much just an identity fn here)
// not sure what the 4th piece of gl_position is yet..., or
// why we say location = 0 (isn't it obvious?)
// according to the book, people often use this step to "normalize"
// coordinates into opengl land (everything is between -1 and 1)

// matrix multiplication is RtL, so even though we want it to go
// our coords (local) -> world -> perspective, instead we do
// perspective -> world -> our coords
// adding in view, so after placing our object into the world, we move
// things so they appear as if you are seeing them from your position
// inside the world (the camera view)
var vertexShaderStr = `
#version 410 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 aTexCoord;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCoord;

void main()
{
	gl_Position = projection*view*model*vec4(pos.x,pos.y,pos.z,1.0);
	texCoord = aTexCoord;
}
` + "\x00"

// right at the end of the opengl pipeline, defines the color
// of each pixel
// pretty chill implementation right now (just a constant)
// im guessing theres a way to actually lookup the right value ???
// so some way to accept an input and compute a color based on it
// but not quite there yet
// RGBA
// i think texture refers to the currently bound opengl texture?

// pre-texture shader
// 	#FragColor = vec4(0.0f,0.0f,1.0f,1.0f);
var fragmentShaderStr = `
#version 410 core
out vec4 FragColor;

in vec2 texCoord;
uniform sampler2D ourTexture;
uniform vec4 lightColor;
void main()
{
	FragColor = lightColor*texture(ourTexture,texCoord);
}
` + "\x00"

var lightSourceShaderStr = `
#version 410 core
out vec4 FragColor;

in vec2 texCoord;
uniform sampler2D ourTexture;
void main()
{
	FragColor = texture(ourTexture,texCoord);
}
` + "\x00"

var frozenVertexShaderStr = `
#version 410 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 aTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCoord;

void main()
{
	gl_Position = projection* view * model * vec4(pos.x,pos.y,pos.z,1.0);
	texCoord = aTexCoord;
}
` + "\x00"

// confirmed in minecraft the sun is directly above you (at least during the day)

// opengl provides a bunch of different coordinate systems before it actually puts pixels on the screen.
// the first coord system is local, which is basically just what you choose to hold the location of your object(s)
// the next coord system is world, which is just where your objects are with respect to each other and the empty space around them
// you can use this to modify how your object is shown, think like having a car basically stored as a bunch of vertices facing you,
// and then using the model matrix to rotate it 45* so you get a better view of it.
// what i'd like to do is set up a cube (as if its facing me), and then rotate that cube 45 degrees on the Y or X axes, the z axis is just
// going to rotate the square, because z is basically coming in to you

func main() {
	runtime.LockOSThread()

	window := initGlfw()
	// when main returns, glfw dies
	defer glfw.Terminate()

	program := initOpenGL()
	// ok, so disabling just about everything except some init functions causes memory to
	// stay constant at about 11kb
	// enabling makescene stays constant at just a tiny bit more (still 11kb)
	// no - it doesn't - golang was being smart and removing world from memory b/c it saw
	// it wasn't used, adding a print keeps it as alloc'd
	// i can see memory growing unbounded, and i disabled the go routines which makes me think
	// something in the load/save is doing it
	makeScene()
	//fmt.Print(world.chunks["0,0"].bounds)
	// guessing this is an infinite loop
	for !window.ShouldClose() {
		render(window, program)
	}
}

// start with a fov of 45 degrees
// does smaller fov mean you're zooming in? feel like it does...
// it does, i want to start smaller, so.....
// think ill just make my world coords smaller
var fov float32 = 45

// why are there two...
// well its the 2nd one
func onMouseScroll(window *glfw.Window, xOffset float64, yOffset float64) {
	fov -= float32(yOffset)
	if fov < 1 {
		fov = 1
	}
	if fov > 45 {
		fov = 45
	}
	updateFrustrum()
}

// could u just get the cursor pos every frame?
// just go center of screen to start ... or not, book just sets these
// to the initial cursor vals
var lastX float32 = -1
var lastY float32 = -1
var mouseSensitivity float32 = 0.01
var firstMouse bool = true

func onMouseMove(window *glfw.Window, xpos float64, ypos float64) {
	// basically orient lastX and lastY on the first cursor change
	if firstMouse {
		lastX = float32(xpos)
		lastY = float32(ypos)
		firstMouse = false
	}
	var xOffset float32 = (float32(xpos) - lastX) * mouseSensitivity
	// book says to reverse here, not quite sure why?
	var yOffset float32 = (lastY - float32(ypos)) * mouseSensitivity
	lastX = float32(xpos)
	lastY = float32(ypos)
	yaw += xOffset
	pitch += yOffset
	// prevent weird flipping, probably constrain this even more to match MC
	if pitch > 89 {
		pitch = 89
	}
	if pitch < -89 {
		pitch = -89
	}
	// not quite sure why i had to switch to the books math from mine, felt like they were equivalent??
	// not feeling the constant flipping between f64 and f32, im probably doing something dumb here
	var x float64 = math.Cos(float64(mgl32.DegToRad(pitch))) * math.Cos(float64(mgl32.DegToRad(yaw))) //float32(math.Sin(float64(mgl32.DegToRad(pitch))) * math.Cos(float64(mgl32.DegToRad(yaw))))
	var y float64 = math.Sin(float64(mgl32.DegToRad(pitch)))                                          //float32(math.Sin(float64(mgl32.DegToRad(pitch))) * math.Sin(float64(mgl32.DegToRad(yaw))))
	var z float64 = math.Sin(float64(mgl32.DegToRad(yaw))) * math.Cos(float64(mgl32.DegToRad(pitch))) //float32(math.Cos(float64(mgl32.DegToRad(pitch))))
	//fmt.Printf("(%f,%f,%f)",x,y,z)
	// aren't they already normalized? not sure why book is saying to normalize here
	cameraFront = (mgl32.Vec3{float32(x), float32(y), float32(z)}).Normalize()
	// forgot to also update the frustrum on mouse move ayyyy
	updateFrustrum()
}

// duh - it returns a pointer to the window - why are go returns after the function ???
func initGlfw() *glfw.Window {
	// if glfw isn't able to startup, give up
	if err := glfw.Init(); err != nil {
		panic(err)
	}
	// Curious to see what making it resizable does, or not
	glfw.WindowHint(glfw.Resizable, glfw.False)

	// No longer guessing, we are on 4.1 :^ )
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	// this makes sense now, its saying lets use big boy opengl, not the old immediate mode opengl
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	// Create a window dude
	window, err := glfw.CreateWindow(width, height, "Gocraft", nil, nil)
	// Again, give up if we can't make a window
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()
	window.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)
	window.SetCursorPosCallback(onMouseMove)
	window.SetScrollCallback(onMouseScroll)
	return window
}

var prog uint32
var lightingProg uint32

// Pretty simple, just starting up opengl, printing out the version
// (guessing its very old) and then doing some stuff??? to create a
// program -> apparently this lets us store shaders?
func initOpenGL() uint32 {
	if err := gl.Init(); err != nil {
		panic(err)
	}
	version := gl.GoStr(gl.GetString(gl.VERSION))
	log.Println("OpenGL Version is: ", version)

	// this makes sense, pretty much we take our strings of
	// custom gl shaders, compile them, and given they worked when
	// compiled we place them into our opengl program context so they
	// get applied to all our pixels. i wonder if theres a way to swap
	// them out/ if people want to do that? if you apply multiple of the
	// same type (like 2 vertex shaders) does it run 1 after the other
	vertexShader, err := compileShader(vertexShaderStr, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}
	fragmentShader, err := compileShader(fragmentShaderStr, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}
	lightSourceShader, err := compileShader(lightSourceShaderStr, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}
	frozenVertexShader, err := compileShader(frozenVertexShaderStr, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}
	prog = gl.CreateProgram()
	lightingProg = gl.CreateProgram()

	gl.AttachShader(prog, vertexShader)
	gl.AttachShader(prog, fragmentShader)
	gl.AttachShader(lightingProg, frozenVertexShader)
	gl.AttachShader(lightingProg, lightSourceShader)

	gl.LinkProgram(prog)
	gl.LinkProgram(lightingProg)
	initSkyShaders()
	initCrossHairShaders()
	// book says to delete them since we attached them, so lets do it
	// im guessing this just frees up some memory, since we fully loaded them
	// then they probably got copied into opengl's program context
	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)
	gl.DeleteShader(lightSourceShader)
	gl.DeleteShader(frozenVertexShader)
	return prog
}

// im having a lot of difficulty understanding the x/z piece of the equation being explained
// by the book. i do, though, understand polar (spherical) coordinates decently well, and the gist of what
// we want to do is define a camera position by two angles (its effectively a vector always length 1, and we only want to move in the xz (yaw) and yz (pitch) planes)
// i think? so pretty much its just figuring out where a point lies on a sphere with radius 1.
// this makes p = 1, so it just goes away in the equations.
// translating spherical coords to the opengl coord system is
// x = sin(phi) * cos(theta)
// y = sin(phi) * sin(theta)
// z = cos(phi)
// z->y, x->z, y->x
// y = cos(phi)
// x = sin(phi) * sin(theta)
// z = sin(phi) * cos(theta)
// WHY ARE THE STUPID SIN/COS REVERSED AHHHHHHHHHHHHHHHHH
// i think these equations are equivalent, i've just defined my coords ending in xz
// instead of starting there (maybe?)
// worked this out on a sheet of paper, its not too bad. basically the camera is pointing at the coordinate system, so the angles are flipped
// if that makes sense.
var yaw float32 = -90
var pitch float32 = 0

var fallTime float32 = 0.0
var jumpTime float32 = 0.0
var jumpVelocity float32 = 6

const acceleration = 5

const (
	STABLE  = 0
	FALLING = 1
	JUMPING = 2
)

var playerState = STABLE

// nice little camera move speed bug
var deltaTime float32
var lastFrame float32

func processInput(window *glfw.Window) {
	// must mean glfw.GetTime() starts at 0 when program starts?
	// basically, calculate the time between last and current frame, and use that
	// as a factor in how much we move the camera
	var currentFrame float32 = float32(glfw.GetTime())
	deltaTime = currentFrame - lastFrame
	lastFrame = currentFrame
	// why don't i need to dereference the window ptr in order to call functions on it??
	// ok, so we need to clamp the speed to prevent us from moving so fast that we skip over chunks that we
	// want to load
	var cameraSpeed float32 = deltaTime * 6
	if cameraSpeed > float32(chunkSize)*cs-1 {
		// i dont want annoying weird rounding errors, so clamp to 1 less
		cameraSpeed = float32(chunkSize)*cs - 1
	}
	// first if statement in go wooooooo
	// move in the x and z directions, no flying for us
	// we want to move the camera "relative" to where we are pointing (cameraFront)
	// why don't we need a X product here? im guessing its because we can't move in the Y?
	// so we'd be crossing into a constant, maybe??
	// FLY FOREVER
	moveVector := mgl32.Vec3{0, 0, 0}
	if window.GetKey(glfw.KeyW) == glfw.Press {
		moveVector = cameraFront.Mul(cameraSpeed)
		// think this will only ever change the z(but maintain the other coords)
		// cameraPos = cameraPos.Add(cameraFront.Mul(cameraSpeed))
	}
	if window.GetKey(glfw.KeyS) == glfw.Press {
		moveVector = moveVector.Add(cameraFront.Mul(-cameraSpeed))
		// ditto
		//cameraPos = cameraPos.Add(cameraFront.Mul(-cameraSpeed))
	}
	// game plan is to use the cross product to compute the actual direction thats to the right/left of us
	// based on where we are pointing
	// again, right now this isn't needed, the cross product is always just going to be a constant of [1,0,0]
	// because its the unchanging cameraFront (0,0,-1) X with cameraUp (0,1,0)
	if window.GetKey(glfw.KeyA) == glfw.Press {
		moveVector = moveVector.Add(cameraFront.Cross(cameraUp).Normalize().Mul(cameraSpeed))
		//cameraPos = cameraPos.Add(cameraFront.Cross(cameraUp).Normalize().Mul(cameraSpeed))
	}
	if window.GetKey(glfw.KeyD) == glfw.Press {
		moveVector = moveVector.Add(cameraFront.Cross(cameraUp).Normalize().Mul(-cameraSpeed))
		//cameraPos = cameraPos.Add(cameraFront.Cross(cameraUp).Normalize().Mul(-cameraSpeed))
	}
	if window.GetKey(glfw.KeySpace) == glfw.Press && playerState == STABLE {
		jumpTime = 0
		playerState = JUMPING
	}
	if window.GetMouseButton(glfw.MouseButtonLeft) == glfw.Press {
		startDestroying(world)
	} else {
		stopDestroying()
	}
	if window.GetMouseButton(glfw.MouseButtonRight) == glfw.Press {
		startBuilding(world, player.x, player.y, player.z, player.width, player.height, player.depth)
	} else {
		stopBuilding()
	}
	// ok, this code seems reasonable but something is off
	// lets render a cube at x+1 or something like that so we can see where we think we are at all times wrt
	// the camera
	// i don't think camerapos properly correctly reflects the position of an object in the world (the way im trying to do it),
	// so im going to create a playerpos
	// i think the camera is looking at a spot cameraFront away, so to properly prevent entering boundaries we shift by that amount
	// cameraPos.X()+moveVector.X(), cameraPos.Y(), cameraPos.Z(), 1, 1, 1
	if !world.intersecting(player.x+moveVector.X(), player.y, player.z, player.width, player.height, player.depth) {
		//cameraPos = cameraPos.Add(mgl32.Vec3{moveVector.X(), 0, 0})
		player.moveX(moveVector.X())
	} // need to think about this a little more, but we should allow clamping to integer coords. this prevents us from being stuck at a "variable"
	// distance< movedspeed from any given wall/block
	//cameraPos.X(), cameraPos.Y(), cameraPos.Z()+moveVector.Z(), 1, 1, 1
	if !world.intersecting(player.x, player.y, player.z+moveVector.Z(), player.width, player.height, player.depth) {
		//cameraPos = cameraPos.Add(mgl32.Vec3{0, 0, moveVector.Z()})
		player.moveZ(moveVector.Z())
	}
	if playerState == JUMPING {
		// if you just apply a constant velocity up, isn't gravity going to slowly decelerate that to 0 until you hit a peak
		// and then you go down?
		jumpTime += deltaTime
		velocity := jumpVelocity - acceleration*jumpTime
		deltaY := velocity * deltaTime
		if velocity <= 0 || world.intersecting(player.x, player.y+deltaY, player.z, player.width, player.height, player.depth) {
			playerState = FALLING
		} else {
			//cameraPos = cameraPos.Add(mgl32.Vec3{0, deltaY, 0})
			player.moveY(deltaY)
		}
	} else if !world.intersecting(player.x, player.y-0.01, player.z, player.width, player.height, player.depth) {
		// going to need to be a little smarter to get gravity
		// (change in velocity)/t = a = 10  - lets just use 10 as our acceleration for now
		// (change in position)/t = v
		// bit of a hack with this -0.01 thing, think about this a little can do it better
		playerState = FALLING
		fallTime += deltaTime
		velocity := fallTime * acceleration
		deltaY := velocity * deltaTime
		if deltaY > 1 {
			deltaY = 1
		}
		if !world.intersecting(player.x, player.y-deltaY, player.z, player.width, player.height, player.depth) {
			//cameraPos = cameraPos.Add(mgl32.Vec3{0, -deltaY, 0})
			player.moveY(-deltaY)
		} else {
			//clamp
			clampY := player.y - float32(math.Floor(float64(player.y))) - 0.005
			//cameraPos = cameraPos.Add(mgl32.Vec3{0, -clampY, 0})
			player.moveY(-clampY)
		}
	} else {
		fallTime = 0
		playerState = STABLE
	}
	updateFrustrum()
	// forgot to record about an hour or 2, but working through moving the loading onto a
	// background thread
	// basically, we want to load another thread up (that will in parallel load or create chunks)
	// but we want to prevent retriggering this thread when its already running, and we want
	// to prevent more than 1 stacking up if we move too "fast"
	// seems sort of dumb to create a goroutine each time in this thread, but they are stupid
	// fast, so not sure it matters. could pick a better pattern to do this maybe (i.e. do we
	// need to make the thread?, then do it), but lets see how this does
	//world.onMove(cameraPos.X(), cameraPos.Z(), textures)
	world.onMove(player.x, player.z, textures)
	crossHairsOnMove(world, projectionMatrix, viewMatrix, player.getCameraPos())
	//fmt.Println(player.x, player.y, player.z)
}

type Plane struct {
	p0 mgl32.Vec3
	p1 mgl32.Vec3
	p2 mgl32.Vec3
}

// not exactly the distance, but the sign is all that matters (which side is it on)
func (plane Plane) getSignedDistance(p mgl32.Vec3) float32 {
	// equation of the plane is calculated as
	// p1-p0 X p2-p0 defines a vector, <A,B,C> (which is normal to the plane)
	// Ax + By +Cz + D = 0
	// distance = Ap.x + Bp.y + Cp.z + D
	var v1 = plane.p1.Sub(plane.p0)
	var v2 = plane.p2.Sub(plane.p0)
	var n = v1.Cross(v2)
	// Just sub any of the 3 points in to calculate D
	var D = -(n.X()*plane.p0.X() + n.Y()*plane.p0.Y() + n.Z()*plane.p0.Z())
	var distance = n.X()*p.X() + n.Y()*p.Y() + n.Z()*p.Z() + D
	return distance
}

var frustrumVao uint32
var frustrumVbo uint32
var planes []Plane

// perfect, frustrum test is now passing. we have successfully defined a pyramid(ish) that contains
// our view box, regardless of how we translate, rotate, or zoom. the test is simple, the far box should always
// be on the edges, no matter what we do. the near box is too small to propely render right, but i thinkkkkkkkk
// its working

// next step is to create the six planes of the frustrum using the 8 points we have, setup a test for
// any point against those planes, and try to apply it to chunks
func updateFrustrum() {
	cameraPos := player.getCameraPos()
	var fd = float32(distanceFar)
	var nd = float32(distanceNear)
	// the tutorial says to compute height off the angle, and then get width off
	// of the ratio relationship. intuitively, it feels like i could do it off width and
	// then get height, but im just going to match the tutorial for now
	var hn = 2 * float32(math.Tan(float64(mgl32.DegToRad(fov/2)))) * nd
	var wn = hn * ratio
	var hf = 2 * float32(math.Tan(float64(mgl32.DegToRad(fov/2)))) * fd
	var wf = hf * ratio

	// i could be wrong on the sign of the direction vector, just try flipping it if its off
	// camerafront is the direction vector, not camerapos
	var dv = cameraFront
	// looks like i was crossing in the wrong direction, just checking off the right hand rule, so i reversed it
	// which fixed the backface culling
	var rv = dv.Cross(cameraUp).Normalize()
	// ok, so the book/tutorial completely botches this, but the uv is not what should be used to calculate your y offsets
	// intuitively, this makes sense because we shouldn't ever be traveling fully in the Y direction (unless we are looking straight on)
	// so the "up" vector isn't the Y vector usually, it needs to be calculated as the cross of the first 2 vectors
	// looking at the actual code written for the tutorial this is done, but it just ignores that fact in the previous section
	var uv = dv.Cross(rv).Normalize()

	var ftl = dv.Mul(fd).Add(uv.Mul(hf / 2)).Sub(rv.Mul(wf / 2)).Add(cameraPos)
	var ftr = dv.Mul(fd).Add(uv.Mul(hf / 2)).Add(rv.Mul(wf / 2)).Add(cameraPos)
	var fbr = dv.Mul(fd).Sub(uv.Mul(hf / 2)).Add(rv.Mul(wf / 2)).Add(cameraPos)
	var fbl = dv.Mul(fd).Sub(uv.Mul(hf / 2)).Sub(rv.Mul(wf / 2)).Add(cameraPos)

	var ntl = dv.Mul(nd).Add(uv.Mul(hn / 2)).Sub(rv.Mul(wn / 2)).Add(cameraPos)
	var ntr = dv.Mul(nd).Add(uv.Mul(hn / 2)).Add(rv.Mul(wn / 2)).Add(cameraPos)
	var nbr = dv.Mul(nd).Sub(uv.Mul(hn / 2)).Add(rv.Mul(wn / 2)).Add(cameraPos)
	var nbl = dv.Mul(nd).Sub(uv.Mul(hn / 2)).Sub(rv.Mul(wn / 2)).Add(cameraPos)

	/*var origin = mgl32.Vec3{0, 0, -250}*/
	// i think the order of points is wrong?
	// when a point is inside the left/right planes, for example, it should score either
	// + or - for both planes (i.e "inside" vs "outside")
	// again, tutorial does a really bad of explaining this, but if i want my normals
	// to all point "in", then i need them to point in opposing directions. for that to happen
	// the point order needs to be reversed
	var lp = Plane{ntl, ftl, fbl} //CW
	var rp = Plane{fbr, ftr, ntr} //CCW
	var ap = Plane{ntl, ntr, ftr} //CCW
	var bp = Plane{fbr, nbr, nbl} //CW
	var np = Plane{nbr, ntr, ntl} //CCW
	var fp = Plane{ftl, ftr, fbr} //CW
	planes = []Plane{lp, rp, ap, bp, np, fp}
	// good stopping point, basically the far frustrum seems correct, the near
	// frustrum isn't quite visible, not sure why
	// realized that my updating of the frustrum is currently wrong, should disable the updating
	// so we can just view the initial frustrum

	// also, i disabled backface culling because somehow i got the directions wrong.
	// manually looking at the points, too, it looks like i screwed something up with directions
	// i.e. first point is +x,+y but it shouldn't be according to what i did here...
	var frustrumVertices = []float32{
		ftl.X(), ftl.Y(), ftl.Z(), //ftl=0
		ftr.X(), ftr.Y(), ftr.Z(), //ftr=1
		fbr.X(), fbr.Y(), fbr.Z(), //fbr=2
		fbl.X(), fbl.Y(), fbl.Z(), //fbl=3

		ntl.X(), ntl.Y(), ntl.Z(), //ntl=4
		ntr.X(), ntr.Y(), ntr.Z(), //ntr=5
		nbr.X(), nbr.Y(), nbr.Z(), //nbr=6
		nbl.X(), nbl.Y(), nbl.Z(), //nbl=7
	}
	var frustrumIndices = []uint32{
		// near face (CCW)
		7, 5, 4,
		7, 6, 5,
		//far face, also CCW, pls dont backface cull me
		3, 1, 0,
		3, 2, 1,
		// for some reason i went far face first aggggggggggg in vertices
		//left face
		2, 1, 5,
		2, 5, 6,
		//right face
		3, 0, 4,
		3, 4, 7,
	}

	if frustrumVao == 0 {
		for p := 0; p < len(planes); p++ {
			// -500,0,-10
			//fmt.Printf("%f\n", planes[p].getSignedDistance(mgl32.Vec3{0, 0, -10}))
		}
		// take the eight chunk points, figure out if they are inside the frustrum using
		// this
		// +,-,+,-,-,+ -> seeems right for <0,0,-250> ?
		/*fmt.Printf(
			"(%f,%f,%f,%f,%f,%f)",
			lp.getSignedDistance(origin),
			rp.getSignedDistance(origin),
			ap.getSignedDistance(origin),
			bp.getSignedDistance(origin),
			np.getSignedDistance(origin),
			fp.getSignedDistance(origin),
		)*/
		var ebo uint32
		gl.GenVertexArrays(1, &frustrumVao)
		gl.BindVertexArray(frustrumVao)
		gl.GenBuffers(1, &frustrumVbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, frustrumVbo)
		gl.BufferData(gl.ARRAY_BUFFER, 4*len(frustrumVertices), gl.Ptr(frustrumVertices), gl.DYNAMIC_DRAW)
		gl.GenBuffers(1, &ebo)
		gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
		gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(frustrumIndices)*4, gl.Ptr(frustrumIndices), gl.DYNAMIC_DRAW)
		gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)
		gl.EnableVertexAttribArray(0)
		//fmt.Print(frustrumVertices)
	} else {
		gl.BindBuffer(gl.ARRAY_BUFFER, frustrumVbo)
		gl.BufferData(gl.ARRAY_BUFFER, 4*len(frustrumVertices), gl.Ptr(frustrumVertices), gl.DYNAMIC_DRAW)
	}
}

func drawFrustrum() {
	gl.BindVertexArray(frustrumVao)
	gl.DrawElements(gl.TRIANGLES, 24, gl.UNSIGNED_INT, nil)
}

const FPS = 60
const nsPerFrame = int64(1 / float32(FPS) * 1000 * 1000 * 1000)

// clear screen, draw stuff on the screen
// hope this prevents the "window is not responding" because now
// we'are actually doing something in the loop
func render(window *glfw.Window, prog uint32) {
	start := time.Now()
	// backface culling
	gl.Enable(gl.CULL_FACE)
	// theoretically, this should stop things rendering if they are behind other things
	// pretty happy i don't have to write this myself
	// didn't really seem to do much so far...
	gl.Enable(gl.DEPTH_TEST)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	drawSky()
	gl.UseProgram(lightingProg)
	gl.Uniform1i(gl.GetUniformLocation(lightingProg, gl.Str("ourTexture"+"\x00")), 0)
	modelLoc := gl.GetUniformLocation(lightingProg, gl.Str("model"+"\x00"))
	gl.UniformMatrix4fv(modelLoc, 1, false, &modelMatrix[0])
	projectionLoc := gl.GetUniformLocation(lightingProg, gl.Str("projection"+"\x00"))
	gl.UniformMatrix4fv(projectionLoc, 1, false, &projectionMatrix[0])
	viewLoc := gl.GetUniformLocation(lightingProg, gl.Str("view"+"\x00"))
	// we want the sun to be in a fixed position, even as we move through the world
	lightingViewMatrix := mgl32.LookAtV(mgl32.Vec3{0, 0, 0}, cameraFront, cameraUp)
	gl.UniformMatrix4fv(viewLoc, 1, false, &lightingViewMatrix[0])

	sun.draw()
	gl.UseProgram(prog)

	// setup the texture - had this in the wrong spot before, can't do this before
	// we call useprogram
	gl.Uniform1i(gl.GetUniformLocation(prog, gl.Str("ourTexture"+"\x00")), 0)
	gl.Uniform4f(gl.GetUniformLocation(prog, gl.Str("lightColor"+"\x00")), 1, 1, 1, 1)
	// wireframe dog
	//gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)

	// listen to keys
	processInput(window)
	cameraPos := player.getCameraPos()
	viewMatrix = mgl32.LookAtV(cameraPos, cameraPos.Add(cameraFront), cameraUp)
	// must be called AFTER useProgram
	// apparently its this simple to put our model matrix into our
	// program.... you just tell opengl which variable it is
	// (in this case model), and then give it a ptr to ur matrix
	// ahhhhh, its not based on the shaders, its based on just the program,
	// which, oddly enough, means variables are global to a program (not to shaders...)
	modelLoc = gl.GetUniformLocation(prog, gl.Str("model"+"\x00"))

	// not totally sure when to use gl.Ptr or just use a literal ptr???
	gl.UniformMatrix4fv(modelLoc, 1, false, &modelMatrix[0])

	// because fov changes, we need to recompute
	// u would think we could do this inside the scroll callback?
	projectionMatrix = mgl32.Perspective(mgl32.DegToRad(fov), float32(width)/float32(height), 0.1, 1000.0)
	projectionLoc = gl.GetUniformLocation(prog, gl.Str("projection"+"\x00"))
	gl.UniformMatrix4fv(projectionLoc, 1, false, &projectionMatrix[0])

	// dbl checkint this time dude
	viewLoc = gl.GetUniformLocation(prog, gl.Str("view"+"\x00"))
	gl.UniformMatrix4fv(viewLoc, 1, false, &viewMatrix[0])

	// makeVao and makeEbo have setup everything for us ctx wise
	// in opengl, its ready to draw some vertices (as defined by us)
	// and apply our shaders to those vertices
	world.draw(planes)
	drawCrossHairs()
	//player.draw()
	//drawFrustrum()
	//printGlError()
	glfw.PollEvents()
	window.SwapBuffers()

	elapsedNs := time.Since(start).Nanoseconds()
	time.Sleep(time.Duration(nsPerFrame - elapsedNs))
}

func printGlError() {
	// get the gl error, print it out (as an int)
	glError := gl.GetError()
	if glError > 0 {
		fmt.Printf("%d", glError)
	}
}

// Starting to understand this stuff better
// There's a pipeline in opengl, it goes:
// * vertex shader -> can write custom shader, just takes in single vertices and "transforms" them, not sure what the transforms are yet, im guessing its for camera type stuff
// * shape assembly -> can't write custom shaders here, but it combines vertices into shapes (3 pts to a triangle, for example)
// * geometry shader -> can write custom shaders here, takes the shape and transforms it, again not sure what this is useful for
// * rasterization -> (no custom shader) convert shapes to pixels
// * fragment shader -> (can custom shade) color those pixels
// * tests and blending -> (no custom shader) figure out if things are behind other things/probably transparency. don't render pixels that are blocked by other pixels
func makeScene() {
	var err error
	textures, err = loadTextures("textures.png")
	cubes = loadCubes(textures)
	player = makePlayer(0, 210, 0, 1, 3, 1, &textures[9])
	sun = makeSun(0, 250, -900, 75, 75, 20, &textures[158])
	makeSky()
	makeCrossHairs(textures[44*18-1])
	// not the right spot for this, just testing to see if it even works
	if err != nil {
		log.Fatal(err)
	}
	updateFrustrum()
	// leak is in here, i believe
	world = loadWorld(cubes)
	// so, think the correct camera position is a cube centered around it, which is why i was so confusede
	// most people probably just use a sphere instead, its more intuitive
	// one issue we still need to work through is i can't quite fit in single block spaces
	//10, 128, 0
}

// Attempt to compile shader string - returns either a pointer to the
// GL shader object or 0 + an error
func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {

		// Load the error into memory fropm GL so we can print it
		// out to the console
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}
	return shader, nil
}
