package main

import (
	"fmt"
	"image"
	"image/draw"
	"os"

	// thank you autocomplete golang thing for putting the wrong version of gl
	"github.com/go-gl/gl/v4.1-core/gl"

	// "register" the png format...

	_ "image/png"
)

type Texture struct {
	x1 float32
	y1 float32
	x2 float32
	y2 float32
}

const textureSheetWidth = 384
const textureSheetHeight = 704
const textureSize = 16

func loadTextures(filename string) ([]Texture, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	// close file after we've made our texturemap
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	// not totally sure why we do this, guessing its to flip the image to y is up coords
	var drawnImage = image.NewRGBA(img.Bounds())
	draw.Draw(drawnImage, img.Bounds(), img, image.Pt(0, 0), draw.Src)
	var texture uint32
	gl.GenTextures(1, &texture)
	// apparently, you can merge together multiple textures by activating more than one. in this
	// case i just want my one texture (containing small sections defining each cube type) to work
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	// first param is saying its a 2d texture, dont mess with 3d or 1d ones
	// second is saying dont do mipmap stuff
	// third is saying to STORE it as an RGBA format - i wonder if i can convert to just RGB, i dont need transparency (yet)
	// 4th and 5th are width and height of the texture
	// sixth is 0 (no reason given)
	// 7th is saying its being SPECIFIED as an RGBA format
	// 8th is how big each channel(r,g,b,a) in a pixel is (0-256, one byte)
	// 9th is a ptr to the literal src image, not sure why i have to use the weird gl.Ptr syntax
	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.SRGB_ALPHA, int32(img.Bounds().Dx()), int32(img.Bounds().Dy()), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(drawnImage.Pix))
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	// not sure why this would be required...
	// well, it is required
	gl.GenerateMipmap(gl.TEXTURE_2D)
	// turns out, we can't pass multiple textures to our chunk vaos, so
	// need to just use one big texture AGGGGGGGGGGG
	// implies image holding all the textures is a square
	// probably don't need to hold this in memory at all...
	xTextures := textureSheetWidth / textureSize
	yTextures := textureSheetHeight / textureSize
	texelWidth := float32(1) / textureSheetWidth
	texelHeight := float32(1) / textureSheetHeight
	var textures = make([]Texture, xTextures*yTextures)
	for i := 0; i < xTextures; i++ {
		for j := 0; j < yTextures; j++ {
			x1 := float32(i) / float32(xTextures)
			y1 := float32(j) / float32(yTextures)
			x2 := float32(i+1)/float32(xTextures) - texelWidth
			y2 := float32(j+1)/float32(yTextures) - texelHeight
			if i*yTextures+j == 10*44+18 {
				fmt.Printf("%f,%f,%f,%f", x1, y1, x2, y2)
			}
			textures[i*yTextures+j] = Texture{x2, y2, x1, y1}
		}
	}
	return textures, nil
}
