package main

import (
	"log"
	"os"
	"runtime"
	"runtime/pprof"
)

func enableCpuProfiling() {
	f, err := os.Create("cpu-profile")
	if err != nil {
		log.Fatal("could not create CPU profile: ", err)
	}
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Fatal("could not start CPU profile: ", err)
	}
	//defer f.Close() // error handling omitted for example
	// defer pprof.StopCPUProfile()
}

func dumpMemory() {
	f, err := os.Create("memprofile")
	if err != nil {
		log.Fatal("could not create memory profile: ", err)
	}
	defer f.Close() // error handling omitted for example
	runtime.GC()    // get up-to-date statistics
	if err := pprof.WriteHeapProfile(f); err != nil {
		log.Fatal("could not write memory profile: ", err)
	}
}
