package main

import (
	"fmt"
	"gocraft/perlin"
)

type ChunkIndex struct {
	x int
	y int
	z int
}

type Cave struct {
	x      int
	y      int
	z      int
	length uint
	seeds  []uint

	chunkIndicesMap map[string][]ChunkIndex
}

func (c *Cave) applyToChunk(chunk *Chunk) {
	if chunkIndices, ok := c.chunkIndicesMap[chunk.key]; ok {
		for i := 0; i < len(c.chunkIndicesMap[chunk.key]); i++ {
			ci := chunkIndices[i]
			chunk.erodeCube(ci.x, ci.z, ci.y)
		}
	}
}

func (c *Cave) erodeSphere(x int, y int, z int, r int) {
	rSquared := r * r
	for i := x - r; i <= x+r; i++ {
		for j := y - r; j <= y+r; j++ {
			for k := z - r; k <= z+r; k++ {
				// distance formula dude
				if (x-i)*(x-i)+(y-j)*(y-j)+(z-k)*(z-k) <= rSquared && j >= 0 && j < 256 {
					// copy-pasted from World...
					chunkX := chunkIndex(i)
					chunkZ := chunkIndex(k)
					chunkKey := fmt.Sprintf("%d,%d", chunkX, chunkZ)
					//fmt.Println(i, k, chunkKey)
					if _, ok := c.chunkIndicesMap[chunkKey]; !ok {
						c.chunkIndicesMap[chunkKey] = make([]ChunkIndex, 0)
					}
					xIndex := i % chunkSize
					zIndex := k % chunkSize
					if chunkX < 0 {
						xIndex = chunkSize - 1 + (i+1)%chunkSize
					}
					if chunkZ < 0 {
						zIndex = chunkSize - 1 + (k+1)%chunkSize
					}
					c.chunkIndicesMap[chunkKey] = append(c.chunkIndicesMap[chunkKey], ChunkIndex{xIndex, j, zIndex})
				}
			}
		}
	}
}

// length = 65
func makeCave(xStart int, yStart int, zStart int, seeds []uint, wormLength uint) *Cave {
	//seeds := []uint{35, 20, 29, 22}
	c := Cave{xStart, yStart, zStart, wormLength, seeds, make(map[string][]ChunkIndex)}
	ppv := uint(4)
	radiusList := perlin.MakeList(seeds[0], wormLength, 3)
	// capital letters export functions from packages for some obscure reason
	radiusPerlin := radiusList.Perlin1D(ppv, 3, 5, 3)
	xList := perlin.MakeList(seeds[1], wormLength, 3)
	xPerlin := xList.Perlin1D(ppv, -6, 6, 3)
	yList := perlin.MakeList(seeds[2], wormLength, 3)
	// slightly favor going down over going up
	yPerlin := yList.Perlin1D(ppv, -6, 3, 3)
	zList := perlin.MakeList(seeds[3], wormLength, 3)
	zPerlin := zList.Perlin1D(ppv, -6, 6, 3)
	x := float32(xStart)
	y := float32(yStart)
	z := float32(zStart)
	for i := 0; i < len(radiusPerlin); i++ {
		x += xPerlin[i]
		z += zPerlin[i]
		// enforce some artificial bounds that reverse the y so we don't get caves that live the majority
		// of their lives in open space
		deltaY := yPerlin[i]
		if int(y) > yStart-10 && deltaY > 0 {
			deltaY = -deltaY
		} else if y < 10 && deltaY < 0 {
			deltaY = -deltaY
		}
		y += deltaY
		c.erodeSphere(int(x), int(y), int(z), int(radiusPerlin[i]))
	}
	//fmt.Print(c.chunkIndicesMap["0,0"])
	return &c
}
