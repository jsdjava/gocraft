package biomes

import (
	"fmt"
	"gocraft/constants"
	"gocraft/perlin"
	"image"
	"image/color"
	"image/png"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"
)

const numBiomes = 5

const (
	WATER = 0
	LAND  = 1

	OCEAN_BIOME     = 0
	PLAINS_BIOME    = 1
	DESERT_BIOME    = 2
	FOREST_BIOME    = 3
	MOUNTAINS_BIOME = 4
)

var colors = []color.Color{
	color.RGBA{0, 0, 255, 255},
	color.RGBA{0, 255, 0, 255},
	color.RGBA{255, 255, 0, 255},
	color.RGBA{165, 42, 42, 255},
	color.RGBA{100, 100, 100, 255},
}

type Biomes struct {
	//biomeGrids   []*perlin.PerlinGrid
	highGrid     *perlin.PerlinGrid
	lowGrid      *perlin.PerlinGrid
	selectorGrid *perlin.PerlinGrid
	biomeWeights []float32

	// TODO Could be a byte obviously
	biomes      map[string]int
	biomesMutex *sync.RWMutex
	layerSeeds  []int
}

// after thinking a little, its clear that the algorithm i implemented here isn't doing what i wanted, basically
// it just creates large contiguous "shapes" that have interesting edges

// im going to try a more controlled/layered approach

func random(seed int, size int) [][]int {
	prevBiomes := make([][]int, 0)
	for i := -size; i <= size+1; i++ {
		prevBiomes = append(prevBiomes, make([]int, 0))
		for j := -size; j <= size+1; j++ {
			randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(seed), i, j))))

			// just pick between water and land for now
			randVal := randsGen.Intn(10)
			if randVal > 3 {
				prevBiomes[i+size] = append(prevBiomes[i+size], LAND)
			} else {
				prevBiomes[i+size] = append(prevBiomes[i+size], WATER)
			}
		}
	}
	return prevBiomes
}

func zoomOut(seed int, prevBiomes [][]int, isFuzzy bool) [][]int {
	curBiomes := make([][]int, 0)
	bounds := len(prevBiomes)
	for i := 0; i < bounds-1; i++ {
		curBiomes = append(curBiomes, make([]int, 0))
		curBiomes = append(curBiomes, make([]int, 0))
		for j := 0; j < bounds-1; j++ {
			randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(seed), i-bounds/2, j-bounds/2))))
			curBiomes[i*2] = append(curBiomes[i*2], prevBiomes[i][j])
			if randsGen.Intn(2) > 0 {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], prevBiomes[i+1][j])
			} else {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], prevBiomes[i][j])
			}
			if randsGen.Intn(2) > 0 {
				curBiomes[i*2] = append(curBiomes[i*2], prevBiomes[i][j+1])
			} else {
				curBiomes[i*2] = append(curBiomes[i*2], prevBiomes[i][j])
			}
			topLeft := prevBiomes[i][j]
			topRight := prevBiomes[i+1][j]
			botLeft := prevBiomes[i][j+1]
			botRight := prevBiomes[i+1][j+1]
			if isFuzzy {
				prevRands := []int{topRight, topLeft, botRight, botLeft}
				curBiomes[i*2+1] = append(curBiomes[i*2+1], prevRands[randsGen.Intn(4)])
				continue
			}
			// theres definitely a less verbose way to do this - the gist is find the "most occurring" of the 4 nearest neighbors,
			// and if theres any ties, then just pick randomly
			if topLeft == topRight && topRight == botLeft {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topLeft == botLeft && botLeft == botRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topLeft == topRight && topRight == botRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topRight == botLeft && botLeft == botRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topRight)
			} else if topLeft == topRight && botLeft != botRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topLeft == botLeft && topRight != botRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topLeft == botRight && topRight != botLeft {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topLeft)
			} else if topRight == botLeft && botRight != topLeft {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topRight)
			} else if topRight == botRight && botLeft != topLeft {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], topRight)
			} else if botLeft == botRight && topLeft != topRight {
				curBiomes[i*2+1] = append(curBiomes[i*2+1], botLeft)
			} else {
				prevRands := []int{topRight, topLeft, botRight, botLeft}
				curBiomes[i*2+1] = append(curBiomes[i*2+1], prevRands[randsGen.Intn(4)])
			}
		}
	}
	return curBiomes
}

func randomOnLand(seed int, prevBiomes [][]int) [][]int {
	bounds := len(prevBiomes)
	curBiomes := make([][]int, 0)
	for i := 0; i < bounds; i++ {
		curBiomes = append(curBiomes, make([]int, 0))
		for j := 0; j < bounds; j++ {
			if prevBiomes[i][j] == LAND {
				randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(seed), i-bounds/2, j-bounds/2))))
				// pick from everything except water
				curBiomes[i] = append(curBiomes[i], randsGen.Intn(numBiomes-1)+1)
			} else {
				curBiomes[i] = append(curBiomes[i], prevBiomes[i][j])
			}
		}
	}
	return curBiomes
}

func sprinkleIslands(seed int, prevBiomes [][]int) [][]int {
	bounds := len(prevBiomes)
	curBiomes := make([][]int, 0)
	for i := 0; i < bounds; i++ {
		curBiomes = append(curBiomes, make([]int, 0))
		for j := 0; j < bounds; j++ {
			if prevBiomes[i][j] == WATER {
				randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(seed), i-bounds/2, j-bounds/2))))
				if randsGen.Intn(30) >= 29 {
					curBiomes[i] = append(curBiomes[i], randsGen.Intn(numBiomes-1)+1)
					continue
				}
			}
			curBiomes[i] = append(curBiomes[i], prevBiomes[i][j])
		}
	}
	return curBiomes
}

func LoopMakeBiomes(layerSeeds []int) [][]int {
	baseBiomes := random(layerSeeds[0], 20)
	zoom1Biomes := zoomOut(layerSeeds[1], baseBiomes, true)
	randLandBiomes := randomOnLand(layerSeeds[2], zoom1Biomes)
	zoom2Biomes := zoomOut(layerSeeds[3], randLandBiomes, true)
	zoom3Biomes := zoomOut(layerSeeds[4], zoom2Biomes, false)
	islandsBiomes := sprinkleIslands(layerSeeds[5], zoom3Biomes)
	zoom4Biomes := zoomOut(layerSeeds[6], islandsBiomes, false)
	return zoom4Biomes
}

// to make this algo work, i think i need to be able to compute it per chunk, so i need
// to sort of "reverse"/ work backwards from a chunk to determine what it becomes

// reversing zoom4 means looking up your 4 "parents"
// reversing islands just means figuring out if the rand generator for your 4 parent chunks will turn you into land (and what land) IF you are water
// reversing zoom3 is looking up the 4 parents for each of those 4 parents
// ditto for zoom2, except as a fuzzy look up

const (
	TL = 0
	TR = 1
	BL = 2
	BR = 3
)

func randomPick(randsGen *rand.Rand, selectorNoise byte, highNoise byte, lowNoise byte) int {
	fmt.Println(selectorNoise, highNoise, lowNoise)
	// just pick between water and land for now
	if selectorNoise <= 50 && lowNoise <= SEA_LEVEL {
		return WATER
	} else if selectorNoise >= 60 && highNoise > 100 {
		return MOUNTAINS_BIOME
	}
	return LAND
}

func islandPick(randsGen *rand.Rand) int {
	if randsGen.Intn(30) >= 28 {
		return randsGen.Intn(3) + 1
	}
	return 0
}

func landPick(randsGen *rand.Rand) int {
	return randsGen.Intn(3) + 1
}

func zoomPick(randsGen *rand.Rand, position int, values []int, isFuzzy bool) int {
	rands := []int{randsGen.Intn(2), randsGen.Intn(2), randsGen.Intn(4)}
	switch position {
	case TL:
		return values[TL]
	case TR:
		if rands[0] > 0 {
			return values[TR]
		}
		return values[TL]
	case BL:
		if rands[1] > 0 {
			return values[BL]
		}
		return values[TL]
	case BR:
		if isFuzzy {
			return values[rands[2]]
		}
		topLeft := values[TL]
		topRight := values[TR]
		botLeft := values[BL]
		botRight := values[BR]

		// theres definitely a less verbose way to do this - the gist is find the "most occurring" of the 4 nearest neighbors,
		// and if theres any ties, then just pick randomly
		if topLeft == topRight && topRight == botLeft {
			return topLeft
		} else if topLeft == botLeft && botLeft == botRight {
			return topLeft
		} else if topLeft == topRight && topRight == botRight {
			return topLeft
		} else if topRight == botLeft && botLeft == botRight {
			return topRight
		} else if topLeft == topRight && botLeft != botRight {
			return topLeft
		} else if topLeft == botLeft && topRight != botRight {
			return topLeft
		} else if topLeft == botRight && topRight != botLeft {
			return topLeft
		} else if topRight == botLeft && botRight != topLeft {
			return topRight
		} else if topRight == botRight && botLeft != topLeft {
			return topRight
		} else if botLeft == botRight && topLeft != topRight {
			return botLeft
		} else {
			return values[rands[2]]
		}
	}
	// shouldn't be possible
	return -1
}

// "decorator" type function
func (b *Biomes) GetCachedBiome(layer int, layerSeeds []int, x int, z int) int {
	key := fmt.Sprintf("%d,%d,%d", layer, x, z)
	b.biomesMutex.RLock()
	if cachedBiome, ok := b.biomes[key]; ok {
		b.biomesMutex.RUnlock()
		return cachedBiome
	}
	b.biomesMutex.RUnlock()
	biome := b.GetBiome(layer, layerSeeds, x, z)
	b.biomesMutex.Lock()
	b.biomes[key] = biome
	b.biomesMutex.Unlock()
	return biome
}

func (b *Biomes) GetBiome(layer int, layerSeeds []int, x int, z int) int {
	switch layer {
	// zoomOut
	case 0:
		fallthrough
	case 2:
		fallthrough
	case 3:
		fallthrough
	case 5:
		isFuzzy := layer == 3 || layer == 5
		nl := layer + 1
		nx := x / 2 //-3 -> -1
		nz := z / 2 //-3 -> -1
		incX := 1
		incZ := 1
		if x < 0 {
			incX = -1
		}
		if z < 0 {
			incZ = -1
		}
		randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(layerSeeds[layer]), nx, nz))))
		tl := b.GetCachedBiome(nl, layerSeeds, nx, nz+incZ) //0,0
		tr := b.GetCachedBiome(nl, layerSeeds, nx+incX, nz+incZ)
		bl := b.GetCachedBiome(nl, layerSeeds, nx, nz)
		br := b.GetCachedBiome(nl, layerSeeds, nx+incX, nz)
		if tl == tr && bl == br && tl == bl && tl == MOUNTAINS_BIOME && layer < 4 {
			return randsGen.Intn(3) + 1
		}
		v := zoomPick(randsGen, getPosition(x, z), []int{tl, tr, bl, br}, isFuzzy)
		return v
	case 1:
		prevBiome := b.GetCachedBiome(2, layerSeeds, x, z)
		randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(layerSeeds[1]), x, z))))
		overrideIsland := islandPick(randsGen)
		if prevBiome == WATER && overrideIsland > 0 {
			return overrideIsland
		}
		return prevBiome
	case 4:
		prevBiome := b.GetCachedBiome(5, layerSeeds, x, z)
		randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(layerSeeds[4]), x, z))))
		overrideLand := landPick(randsGen)
		if prevBiome == LAND {
			return overrideLand
		}
		return prevBiome
	case 6:
		randsGen := rand.New(rand.NewSource(int64(perlin.ChunkHash(uint(layerSeeds[6]), x, z))))
		return randomPick(randsGen, b.selectorGrid.GetPixel(x*16*16, z*16*16, 4), b.highGrid.GetPixel(x*16*16, z*16*16, 4), b.lowGrid.GetPixel(x*16*16, z*16*16, 4))
	}
	// shouldn't be possible
	return -1
}

func getPosition(x int, z int) int {
	if z%2 == 0 {
		if x%2 == 0 {
			return BL
		}
		return BR
	}
	if x%2 == 0 {
		return TL
	}
	return TR
}

func liColor(c1 color.Color, c2 color.Color, index int) color.Color {
	r1, g1, b1, _ := c1.RGBA()
	r2, g2, b2, _ := c2.RGBA()

	//literally whyyyyyyyyyyyyyyyyy
	r1 = r1 >> 8
	g1 = g1 >> 8
	b1 = b1 >> 8
	r2 = r2 >> 8
	g2 = g2 >> 8
	b2 = b2 >> 8
	return color.RGBA{
		li(byte(r1), byte(r2), index),
		li(byte(g1), byte(g2), index),
		li(byte(b1), byte(b2), index),
		255,
	}
}

func li(from byte, to byte, index int) byte {
	if index >= 0 && index <= 7 {
		//move from all me to 1/2 me
		f := float64(index+8) / float64(15)
		return byte(float64(to)*(f) + float64(from)*(1-f))
	} else if index >= 8 && index <= 15 {
		f := float64(index-8) / float64(15)
		return byte(float64(from)*(1-f) + float64(to)*(f))
	}
	return 1
}

func genBicubicLiTable(max int) []float32 {
	table := make([]float32, max+1)
	for i := 0; i <= max; i++ {
		f := float64(i) / float64(max)
		v := f * f * f * (6*f*f - 15*f + 10)
		table[i] = float32(math.Round(float64(v*100000)) / 100000)
	}
	fmt.Println(table)
	return table
}

var biCubicLiTable []float32 = genBicubicLiTable(15)

// so, there can be minor floating point differences (see IEEE blah blah standard about) it,
// but these very tiny differences show up as annoying grid lines, so just going to force the reciprocals to be equal
// by defining both at the same time
// 1/15,2/15,3/15,...14/15,15/15
// edges, be like 0.2
// closer to center, be as much as 5
func biCubicLi(from byte, to byte, index int) byte {
	//centerDist := float32(index % 8)
	if index >= 0 && index <= 7 {
		//move from all me to 1/2 me
		f := biCubicLiTable[index+8] //(float32(index + 8)) / float32(15)
		return byte(float32(to)*(f) + float32(from)*(1-f))
	} else if index >= 8 && index <= 15 {
		f := biCubicLiTable[index-8] //(float32(index - 8)) / float32(15)
		return byte(float32(from)*(1-f) + float32(to)*(f))
	}
	return 1
}
func liBiome(from int, to int, index int, liRand float32) int {
	f := float32(0)
	if index >= 0 && index <= 7 {
		f = (float32(index + 8)) / float32(15)
	} else if index >= 8 && index <= 15 {
		f = (float32(index - 8)) / float32(15)
	}
	if f > liRand {
		return to
	}
	return from
}

func mergeColors(c1 color.Color, c2 color.Color) color.Color {
	r1, g1, b1, _ := c1.RGBA()
	r2, g2, b2, _ := c2.RGBA()

	//literally whyyyyyyyyyyyyyyyyy
	r1 = r1 >> 8
	g1 = g1 >> 8
	b1 = b1 >> 8
	r2 = r2 >> 8
	g2 = g2 >> 8
	b2 = b2 >> 8
	return color.RGBA{
		uint8((r1 + r2) / 2),
		uint8((g1 + g2) / 2),
		uint8((b1 + b2) / 2),
		255,
	}
}

func (b *Biomes) GetPixelColorAndAlpha(layer int, layerSeeds []int, x int, z int, chunkX int, chunkZ int) (color.Color, byte) {
	me := b.GetCachedBiome(layer, layerSeeds, chunkX, chunkZ)
	return colors[me], b.GetPerlinHeight(x, z, me)
	top := b.GetCachedBiome(layer, layerSeeds, chunkX, chunkZ+1)
	bottom := b.GetCachedBiome(layer, layerSeeds, chunkX, chunkZ-1)
	left := b.GetCachedBiome(layer, layerSeeds, chunkX-1, chunkZ)
	right := b.GetCachedBiome(layer, layerSeeds, chunkX+1, chunkZ)
	dtl := b.GetCachedBiome(layer, layerSeeds, chunkX-1, chunkZ+1)
	dtr := b.GetCachedBiome(layer, layerSeeds, chunkX+1, chunkZ+1)
	dbl := b.GetCachedBiome(layer, layerSeeds, chunkX-1, chunkZ-1)
	dbr := b.GetCachedBiome(layer, layerSeeds, chunkX+1, chunkZ-1)

	mePerlin := b.GetPerlinHeight(x, z, me)
	if me == top && me == bottom && me == left && me == right && me == dtl && me == dtr && me == dbl && me == dbr {
		return colors[me], mePerlin
	}
	topPerlin := b.GetPerlinHeight(x, z, top)
	bottomPerlin := b.GetPerlinHeight(x, z, bottom)
	leftPerlin := b.GetPerlinHeight(x, z, left)
	rightPerlin := b.GetPerlinHeight(x, z, right)
	dtlPerlin := b.GetPerlinHeight(x, z, dtl)
	dtrPerlin := b.GetPerlinHeight(x, z, dtr)
	dblPerlin := b.GetPerlinHeight(x, z, dbl)
	dbrPerlin := b.GetPerlinHeight(x, z, dbr)
	// as x moves to the center, we get closer to picking me
	// as y moves to the center, we get closer to picking me
	// on left half, we interpolate left
	// on right half, right,
	// etc
	xIndex := x % 16
	zIndex := z % 16

	if chunkX < 0 {
		xIndex = ((x + 1) % 16) + 15
	}
	if chunkZ < 0 {
		zIndex = ((z + 1) % 16) + 15
	}
	interpolatedValue := byte(0)
	var interpolatedColor color.Color
	if xIndex < 8 {
		if zIndex >= 8 {
			//quadrant 1
			topLiColor := liColor(colors[dtl], colors[top], xIndex)
			botLiColor := liColor(colors[left], colors[me], xIndex)
			topLi := biCubicLi(dtlPerlin, topPerlin, xIndex)
			botLi := biCubicLi(leftPerlin, mePerlin, xIndex)
			interpolatedColor = liColor(botLiColor, topLiColor, zIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)
		} else {
			// quadrant 4
			topLiColor := liColor(colors[left], colors[me], xIndex)
			botLiColor := liColor(colors[dbl], colors[bottom], xIndex)
			topLi := biCubicLi(leftPerlin, mePerlin, xIndex)
			botLi := biCubicLi(dblPerlin, bottomPerlin, xIndex)
			interpolatedColor = liColor(botLiColor, topLiColor, zIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)
		}
	} else {
		if zIndex >= 8 {
			// quadrant 2
			topLiColor := liColor(colors[top], colors[dtr], xIndex)
			botLiColor := liColor(colors[me], colors[right], xIndex)
			topLi := biCubicLi(topPerlin, dtrPerlin, xIndex)
			botLi := biCubicLi(mePerlin, rightPerlin, xIndex)
			interpolatedColor = liColor(botLiColor, topLiColor, zIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)
		} else {
			// quadrant 3
			topLiColor := liColor(colors[me], colors[right], xIndex)
			botLiColor := liColor(colors[bottom], colors[dbr], xIndex)
			topLi := biCubicLi(mePerlin, rightPerlin, xIndex)
			botLi := biCubicLi(bottomPerlin, dbrPerlin, xIndex)
			interpolatedColor = liColor(botLiColor, topLiColor, zIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)
		}
	}
	return interpolatedColor, interpolatedValue
}

/*func (b *Biomes) GetHeightV1(x int, z int, chunkX int, chunkZ int) byte {
	me := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ)
	top := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ+1)
	bottom := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ-1)
	left := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ)
	right := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ)
	dtl := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ+1)
	dtr := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ+1)
	dbl := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ-1)
	dbr := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ-1)

	mePerlin := b.biomeGrids[me].GetPixel(x, z, 4)
	if me == top && me == bottom && me == left && me == right && me == dtl && me == dtr && me == dbl && me == dbr {
		return mePerlin
	}
	topPerlin := b.biomeGrids[top].GetPixel(x, z, 4)
	bottomPerlin := b.biomeGrids[bottom].GetPixel(x, z, 4)
	leftPerlin := b.biomeGrids[left].GetPixel(x, z, 4)
	rightPerlin := b.biomeGrids[right].GetPixel(x, z, 4)
	dtlPerlin := b.biomeGrids[dtl].GetPixel(x, z, 4)
	dtrPerlin := b.biomeGrids[dtr].GetPixel(x, z, 4)
	dblPerlin := b.biomeGrids[dbl].GetPixel(x, z, 4)
	dbrPerlin := b.biomeGrids[dbr].GetPixel(x, z, 4)
	// as x moves to the center, we get closer to picking me
	// as y moves to the center, we get closer to picking me
	// on left half, we interpolate left
	// on right half, right,
	// etc
	xIndex := x % 16
	zIndex := z % 16

	if chunkX < 0 {
		xIndex = ((x + 1) % 16) + 15
	}
	if chunkZ < 0 {
		zIndex = ((z + 1) % 16) + 15
	}

	interpolatedValue := byte(0)
	if xIndex < 8 {
		if zIndex >= 8 {
			//quadrant 1
			topLi := li(dtlPerlin, topPerlin, xIndex)
			botLi := li(leftPerlin, mePerlin, xIndex)
			interpolatedValue = li(botLi, topLi, zIndex)
		} else {
			// quadrant 4
			topLi := li(leftPerlin, mePerlin, xIndex)
			botLi := li(dblPerlin, bottomPerlin, xIndex)
			interpolatedValue = li(botLi, topLi, zIndex)
		}
	} else {
		if zIndex >= 8 {
			// quadrant 2
			topLi := li(topPerlin, dtrPerlin, xIndex)
			botLi := li(mePerlin, rightPerlin, xIndex)
			interpolatedValue = li(botLi, topLi, zIndex)
		} else {
			// quadrant 3
			topLi := li(mePerlin, rightPerlin, xIndex)
			botLi := li(bottomPerlin, dbrPerlin, xIndex)
			interpolatedValue = li(botLi, topLi, zIndex)
		}
	}
	return interpolatedValue
}*/

func (b *Biomes) GetPerlinHeight(x int, z int, biome int) byte {
	if biome == OCEAN_BIOME {
		return SEA_LEVEL
	}
	ratio := (float32(b.selectorGrid.GetPixel(x, z, 4)) / 100) * b.biomeWeights[biome]
	return byte(float32(b.lowGrid.GetPixel(x, z, 4))*(1-ratio) + float32(b.highGrid.GetPixel(x, z, 4))*(ratio))
}

func (b *Biomes) GetHeightAndBiome(x int, z int, chunkX int, chunkZ int, blendRand float32) (byte, byte) {
	me := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ)
	top := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ+1)
	bottom := b.GetCachedBiome(0, b.layerSeeds, chunkX, chunkZ-1)
	left := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ)
	right := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ)
	dtl := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ+1)
	dtr := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ+1)
	dbl := b.GetCachedBiome(0, b.layerSeeds, chunkX-1, chunkZ-1)
	dbr := b.GetCachedBiome(0, b.layerSeeds, chunkX+1, chunkZ-1)
	mePerlin := b.GetPerlinHeight(x, z, me)
	if me == top && me == bottom && me == left && me == right && me == dtl && me == dtr && me == dbl && me == dbr {
		return mePerlin, byte(me)
	}
	topPerlin := b.GetPerlinHeight(x, z, top)
	bottomPerlin := b.GetPerlinHeight(x, z, bottom)
	leftPerlin := b.GetPerlinHeight(x, z, left)
	rightPerlin := b.GetPerlinHeight(x, z, right)
	dtlPerlin := b.GetPerlinHeight(x, z, dtl)
	dtrPerlin := b.GetPerlinHeight(x, z, dtr)
	dblPerlin := b.GetPerlinHeight(x, z, dbl)
	dbrPerlin := b.GetPerlinHeight(x, z, dbr)
	// as x moves to the center, we get closer to picking me
	// as y moves to the center, we get closer to picking me
	// on left half, we interpolate left
	// on right half, right,
	// etc
	xIndex := x % 16
	zIndex := z % 16

	if chunkX < 0 {
		xIndex = ((x + 1) % 16) + 15
	}
	if chunkZ < 0 {
		zIndex = ((z + 1) % 16) + 15
	}

	interpolatedValue := byte(0)
	interpolatedBiome := byte(0)
	if xIndex < 8 {
		if zIndex >= 8 {
			//quadrant 1
			topLi := biCubicLi(dtlPerlin, topPerlin, xIndex)
			botLi := biCubicLi(leftPerlin, mePerlin, xIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)

			topBiomeLi := liBiome(dtl, top, xIndex, blendRand)
			botBiomeLi := liBiome(left, me, xIndex, blendRand)
			interpolatedBiome = byte(liBiome(botBiomeLi, topBiomeLi, zIndex, blendRand))
		} else {
			// quadrant 4
			topLi := biCubicLi(leftPerlin, mePerlin, xIndex)
			botLi := biCubicLi(dblPerlin, bottomPerlin, xIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)

			topBiomeLi := liBiome(left, me, xIndex, blendRand)
			botBiomeLi := liBiome(dbl, bottom, xIndex, blendRand)
			interpolatedBiome = byte(liBiome(botBiomeLi, topBiomeLi, zIndex, blendRand))
		}
	} else {
		if zIndex >= 8 {
			// quadrant 2
			topLi := biCubicLi(topPerlin, dtrPerlin, xIndex)
			botLi := biCubicLi(mePerlin, rightPerlin, xIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)

			topBiomeLi := liBiome(top, dtr, xIndex, blendRand)
			botBiomeLi := liBiome(me, right, xIndex, blendRand)
			interpolatedBiome = byte(liBiome(botBiomeLi, topBiomeLi, zIndex, blendRand))
		} else {
			// quadrant 3
			topLi := biCubicLi(mePerlin, rightPerlin, xIndex)
			botLi := biCubicLi(bottomPerlin, dbrPerlin, xIndex)
			interpolatedValue = biCubicLi(botLi, topLi, zIndex)

			topBiomeLi := liBiome(me, right, xIndex, blendRand)
			botBiomeLi := liBiome(bottom, dbr, xIndex, blendRand)
			interpolatedBiome = byte(liBiome(botBiomeLi, topBiomeLi, zIndex, blendRand))
		}
	}
	return interpolatedValue, interpolatedBiome
}

// i hate golangs stupid package import rules so much holy just let me use the stupid constant
// inside a folder ahhhhhhhhhhh
//var BiomeToCubeIndex = map[int]byte{
//	WATER: DIRT_CUBE_INDEX,
//}
const SEA_LEVEL = byte(66)
const BEDROCK_MAX_LEVEL = 4

// TODO This comes from the highest biome weight and the max perlin value
const MAX_BLOCK_HEIGHT = 170 * 0.7
const TOP_BLOCK_BASE_HEIGHT = 30

func GetMineralTypeAndHeights(mineralHeight int, cubeTypeRand float32, cubeHeightRand float32) (byte, []int) {
	randsGen := rand.New(rand.NewSource(int64(cubeHeightRand * math.MaxUint64)))
	numHeights := randsGen.Intn(50)
	heights := make([]int, numHeights)
	for i := 0; i < numHeights; i++ {
		heights[i] = randsGen.Intn(mineralHeight+1-BEDROCK_MAX_LEVEL) + BEDROCK_MAX_LEVEL
	}
	cubeType := constants.STONE_CUBE_INDEX
	if cubeTypeRand > 0.01 && cubeTypeRand <= 0.02 {
		cubeType = constants.GOLD_CUBE_INDEX
	}
	if cubeTypeRand > 0.02 && cubeTypeRand <= 0.03 {
		cubeType = constants.DIAMOND_CUBE_INDEX
	}
	if cubeTypeRand > 0.03 && cubeTypeRand <= 0.04 {
		cubeType = constants.REDSTONE_CUBE_INDEX
	}
	if cubeTypeRand > 0.2 && cubeTypeRand <= 0.23 {
		cubeType = constants.IRON_CUBE_INDEX
	}
	if cubeTypeRand > 0.1 && cubeTypeRand <= 0.14 {
		cubeType = constants.LAPIS_CUBE_INDEX
	}
	if cubeTypeRand > 0.6 && cubeTypeRand <= 0.7 {
		cubeType = constants.COAL_CUBE_INDEX
	}
	return cubeType, heights
}

func (biomes *Biomes) FillCubeColumn(
	x int, z int, chunkX int, chunkZ int,
	index int,
	cubeMixRand float32, blendRand float32, cubeTypeRand float32, cubeHeightRand float32,
	filled []byte, cubeIndices []byte,
) {
	actualBiome := biomes.GetCachedBiome(0, biomes.layerSeeds, chunkX, chunkZ)
	height, blendedBiome := biomes.GetHeightAndBiome(x, z, chunkX, chunkZ, blendRand)
	bedrockLevel := byte(float32(height) / float32(MAX_BLOCK_HEIGHT) * float32(BEDROCK_MAX_LEVEL))
	topBlockHeight := byte(float32(height) - TOP_BLOCK_BASE_HEIGHT*cubeMixRand)
	for i := byte(0); i <= byte(math.Max(float64(height), float64(SEA_LEVEL))); i++ {
		filled[index+int(i)] = 1
		if i <= bedrockLevel {
			cubeIndices[index+int(i)] = constants.BEDROCK_CUBE_INDEX
			continue
		}
		if i <= SEA_LEVEL && height <= SEA_LEVEL {
			cubeIndices[index+int(i)] = constants.WATER_CUBE_INDEX
			continue
		}
		if actualBiome == MOUNTAINS_BIOME {
			topBlockHeight = height
			cubeIndices[index+int(i)] = constants.STONE_CUBE_INDEX
			continue
		}
		switch blendedBiome {
		case OCEAN_BIOME:
			cubeIndices[index+int(i)] = constants.SAND_CUBE_INDEX
		case PLAINS_BIOME:
			if i == height {
				cubeIndices[index+int(i)] = constants.GRASS_CUBE_INDEX
			} else if i >= topBlockHeight {
				cubeIndices[index+int(i)] = constants.DIRT_CUBE_INDEX
			} else {
				cubeIndices[index+int(i)] = constants.STONE_CUBE_INDEX
			}
		case DESERT_BIOME:
			if i >= topBlockHeight {
				cubeIndices[index+int(i)] = constants.SAND_CUBE_INDEX
			} else {
				cubeIndices[index+int(i)] = constants.STONE_CUBE_INDEX
			}
		case FOREST_BIOME:
			if i == height {
				cubeIndices[index+int(i)] = constants.GRASS_CUBE_INDEX
			} else if i >= topBlockHeight {
				cubeIndices[index+int(i)] = constants.DIRT_CUBE_INDEX
			} else {
				cubeIndices[index+int(i)] = constants.STONE_CUBE_INDEX
			}
		case MOUNTAINS_BIOME:
			if i >= topBlockHeight {
				cubeIndices[index+int(i)] = constants.STONE_CUBE_INDEX
			} else {
				cubeIndices[index+int(i)] = constants.DIRT_CUBE_INDEX
			}
		}
	}
	mineralCubeIndex, mineralHeights := GetMineralTypeAndHeights(int(topBlockHeight), cubeTypeRand, cubeHeightRand)
	for i := 0; i < len(mineralHeights); i++ {
		cubeIndices[index+mineralHeights[i]] = mineralCubeIndex
	}
}

func MakeBiomes() *Biomes {
	b := Biomes{}
	b.biomesMutex = &sync.RWMutex{}
	b.biomes = make(map[string]int)
	b.biomeWeights = []float32{
		0.0,
		0.2, //0.2
		0.4, //0.3
		0.5, //0.4
		0.9, //0.7
	}
	//b.biomeGrids = make([]*perlin.PerlinGrid, numBiomes)
	/*b.biomeGrids[OCEAN_BIOME] = perlin.MakeGrid(4, 9, 1000, 60, 200)
	b.biomeGrids[PLAINS_BIOME] = perlin.MakeGrid(4, 9, 1000, 60, 200)
	b.biomeGrids[FOREST_BIOME] = perlin.MakeGrid(4, 9, 1000, 60, 200)
	b.biomeGrids[DESERT_BIOME] = perlin.MakeGrid(4, 9, 1000, 60, 200)
	b.biomeGrids[MOUNTAINS_BIOME] = perlin.MakeGrid(4, 9, 1000, 60, 200)*/
	b.highGrid = perlin.MakeGrid(4, 6, 1000, 50, 170)
	b.lowGrid = perlin.MakeGrid(4, 10, 1001, 50, 100)
	b.selectorGrid = perlin.MakeGrid(4, 8, 1002, 0, 100)
	b.layerSeeds = []int{
		23, 90, 89, 73, 57, 17, 99,
	}
	return &b
}

const IMG_SIZE = 50 //130

func LightenColor(c color.Color, percentage int) color.Color {
	p := uint32(percentage)
	r, g, b, _ := c.RGBA()

	//literally whyyyyyyyyyyyyyyyyy
	r = r >> 8
	g = g >> 8
	b = b >> 8
	return color.RGBA{uint8(r + ((255 - r) * p / 100)), uint8(g + ((255 - g) * p / 100)), uint8(b + ((255 - b) * p / 100)), 255}
}

func biomeTest() {
	img := image.NewRGBA(image.Rectangle{image.Pt(0, 0), image.Pt(IMG_SIZE*16*2+1, IMG_SIZE*16*2+1)})
	start := time.Now()
	biomes := MakeBiomes()
	for i := -IMG_SIZE; i <= IMG_SIZE; i++ {
		for j := -IMG_SIZE; j <= IMG_SIZE; j++ {
			for x := 0; x < 16; x++ {
				for y := 0; y < 16; y++ {
					b := biomes.GetCachedBiome(0, biomes.layerSeeds, i, j)
					c, p := biomes.GetPixelColorAndAlpha(0, biomes.layerSeeds, i*16+x, j*16+y, i, j)
					if p <= SEA_LEVEL {
						c = color.RGBA{0, 0, 255, 255}
					} else if b == OCEAN_BIOME {
						c = color.RGBA{255, 0, 255, 255}
					}
					if i == 0 && j == 0 {
						c = color.RGBA{0, 0, 0, 255}
					}
					img.Set(i*16+x+IMG_SIZE*16, j*16+y+IMG_SIZE*16, c)
				}
			}
		}
	}
	elapsed := time.Since(start)
	fmt.Print(elapsed)
	f, _ := os.Create("biomes.png")
	png.Encode(f, img)
}

func main() {
	biomeTest()
}
